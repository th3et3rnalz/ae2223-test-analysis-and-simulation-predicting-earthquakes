from matplotlib.ticker import FormatStrFormatter

from _03_data_analysis import DFF
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

class Plot_LR:
    def __init__(self, dataframe, style='fast'):
        self.dataframe = dataframe

    def plot_lr(self, station_name):
        station = self.dataframe.copy()[self.dataframe['name'] == station_name]

        data1 = station['lon (deg E) (m)']
        data2 = station['lat (deg N) (m)']
        data3 = station['rad (m)']

        dates = station.index

        fig, axs = plt.subplots(3, figsize=(10, 5))
        fig.tight_layout(pad=2.0)

        date = pd.to_datetime('2004-12-26')
        zero_measure = station.iloc[station.index.get_loc(date, method='ffill')]
        zero_measure_lon = float(zero_measure[3])
        zero_measure_lat = float(zero_measure[1])
        zero_measure_height = float(zero_measure[5])

        axs[0].plot(dates, (data1 - zero_measure_lon) * 10 ** 7, 's', markersize=0.5)
        axs[1].plot(dates, (data2 - zero_measure_lat) * 10 ** 7, 's', markersize=0.5)
        axs[2].plot(dates, (data3 - zero_measure_height) * 10 ** 2, 's', markersize=0.5)

        amount_of_days = (station.index[-1] - station.index[0]).days
        print(amount_of_days)
        time = np.arange(1, amount_of_days+1, 1)

        # print(DFF.regressors['PHUK']["lon"].predict(time))
        # print(zero_measure_lon)
        # print(DFF.regressors['PHUK']["lat"].predict(time))
        # print(zero_measure_lat)

        # axs[0].plot(pd.date_range(station.index[0], periods = amount_of_days, freq ='D'),
        #             (DFF.regressors[station_name]["lon"].predict(time) - zero_measure_lon) *10**7, alpha=0.4)
        # axs[1].plot(pd.date_range(station.index[0], periods=amount_of_days, freq='D'),
        #             (DFF.regressors[station_name]["lat"].predict(time) - zero_measure_lat) *10**7, alpha=0.4)

        axs[0].plot(pd.date_range(station.index[0], periods=amount_of_days, freq='D'),
                    DFF.regressors[station_name]["lon"].predict(time), alpha=0.4)
        axs[1].plot(pd.date_range(station.index[0], periods=amount_of_days, freq='D'),
                    DFF.regressors[station_name]["lat"].predict(time), alpha=0.4)

        fig.suptitle(station_name)

        plt.xlabel('Time')
        axs[0].set_ylabel('Longitude (cm)')
        axs[1].set_ylabel('Latitude (cm)')
        axs[2].set_ylabel('Height (cm)')

        axs[0].ticklabel_format(useOffset=False, axis='y')
        axs[0].yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        axs[1].ticklabel_format(useOffset=False, axis='y')
        axs[1].yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        axs[2].ticklabel_format(useOffset=False, axis='y')
        axs[2].yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

        axs[0].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)
        axs[1].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)
        axs[2].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)

        axs[0].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)
        axs[1].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)
        axs[2].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)

        axs[0].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)
        axs[1].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)
        axs[2].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)

        plt.savefig('_04_visualisations/Figures/%s_with_lr.png' % station_name, bbox_inches='tight')
        plt.show()
