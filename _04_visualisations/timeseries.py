import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt

class Timeseries:
    def timeserieslongplot(self, name_of_dataframe=""):
        fig, axs = plt.subplots(2)
        axs[0].plot(self.index, self["lat (deg N) (m)"], linewidth=0.2)
        axs[0].set_ylabel("lat (deg N) (m)")
        axs[1].plot(self.index, self["lon (deg E) (m)"], linewidth=0.2)
        axs[1].set_ylabel("lon (deg E) (m)")

        axs[0].set_title("timeseries of {}".format(name_of_dataframe))
        plt.show()






