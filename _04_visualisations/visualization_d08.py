from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

class DataVisualization_d08():
    # def __init__(self):
    #     #self.dataframe = dataframe


    # def time_series(self):
    #     pass

    def map(self, data):
        station_names = []
        station_lons = []
        station_lats = []
        station_lons_end = []
        station_lats_end = []

        # file_path = '_01_data_ingestion/data/datasetigs08/Daily/coords.txt'
        # with open(file_path) as f:
        #     for line in f:
        #         fields = line.split(",")
        #         name = fields[0]
        #         lon = fields[1]
        #         lat = fields[2]
        #         lat = lat.rstrip("\n")
        #         station_names.append(name)
        #         station_lons.append(float(lon))
        #         station_lats.append(float(lat))
        #
        # # print(station_names)
        # #print(station_lons)






        fig = plt.figure(num=None, figsize=(12, 8))
        #map1 = Basemap(width=37000000, height=35000000, resolution='h', projection='aea', lat_1=1., lat_2=1, lon_0=0,lat_0=0)
        map1 = Basemap(projection='merc', llcrnrlat=-80, urcrnrlat=80, llcrnrlon=-180, urcrnrlon=180, resolution='c')
        #map1 = Basemap(projection='merc', llcrnrlat=-80,urcrnrlat=80,llcrnrlon=-180,urcrnrlon=180, resolution='h')
        map1.drawcoastlines(linewidth=0.5)
        map1.fillcontinents(color='tan', lake_color='lightblue')
        # draw parallels and meridians.
        #map1.drawparallels(np.arange(-90., 91., 15.), labels=[True, True, False, False], dashes=[2, 2])
        #map1.drawmeridians(np.arange(-180., 181., 15.), labels=[False, False, False, True], dashes=[2, 2])
        map1.drawmapboundary(fill_color='lightblue')
        #map1.drawcountries(linewidth=1, linestyle='solid', color='k')


        list_of_keys = sorted(list(data['name'].unique()))

        #print(data.copy()[data['name'] == list_of_keys[22]])

        for i in range(len(list_of_keys)):
            station = data.copy()[data['name'] == list_of_keys[i]]

            first_data_point = station.iloc[0]
            lat_check = first_data_point[3]
            if lat_check > 180:
                first_data_point[3] = 360 - lat_check
            station_lons.append(first_data_point[3])
            station_lats.append(first_data_point[1])

        x, y = map1(station_lons, station_lats)
        map1.plot(x, y, 'bo', markersize=5)

        # for label, xpt, ypt in zip(list_of_keys, x, y):
        #     plt.text(xpt + 300000, ypt - 200000, label, fontsize=7)


        for i in range(len(list_of_keys)):
            station = data.copy()[data['name'] == list_of_keys[i]]

            last_data_point = station.iloc[-1]
            lat_check = last_data_point[3]
            if lat_check>180:
                last_data_point[3] = 360-lat_check
            station_lons_end.append(last_data_point[3])
            station_lats_end.append(last_data_point[1])

        #print(station_lons_end)

        for i in range(len(station_lats)):

            x, y = map1(station_lons[i], station_lats[i])

            x2, y2 = map1(station_lons_end[i], station_lats_end[i])

            print(x2-x)

            plt.arrow(x, y, (x2-x)*1000000, (y2-y)*1000000, fc="k", ec="k",
                      head_width=400000, head_length=400000)


