from _04_visualisations.simple import SimplePlot, AllDataPlots
from _04_visualisations.visualization import DataVisualization
from _04_visualisations.visualization_d08 import DataVisualization_d08
from _04_visualisations.timeseries import Timeseries
from _04_visualisations.stations_with_lr import Plot_LR


def plot_all(dataframe):
    # plotting = visualizations.SimplePlot(dataframe=cov.covariance_matrix(data.data_08)['ALGO'])
    # plotting = visualizations.AllDataPlots(d14)
    # plotting.plot_all_stations()
    #plotting = DataVisualization()
    #plotting.map_specific_time(dataframe)
    # plotting = visualizations.AllDataPlots(d14)
    # plotting.plot_all_stations()

    # plotting.plot_x_vs_y()
    # plotting.timeseries(axis='longitude')
    # plotting.timeseries(axis='latitude')
    # plotting.timeseries(axis='height')
    # plotting.plot_two_axis_3d(axis_1='lat_weekly_shift', axis_2='lon_weekly_shift')

    # d14 = data.data_14
    # d14['ARAU']['lat_weekly_shift'].rolling(10).mean().plot()
    #
    # plotting = DataVisualization()
    # plotting.map_specific_time(dataframe)

    # plotting = AllDataPlots(dataframe)
    # plotting.plot_all_stations()

    plotting = Plot_LR(dataframe)
    plotting.plot_lr('PHUK')



    # Plotting time series from d8 and d14
    # visualizations.Timeseries.timeserieslongplot(d14, name_of_dataframe="d14")
    return 'Ok'