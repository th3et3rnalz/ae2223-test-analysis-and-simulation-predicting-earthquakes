from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt

class DataVisualization():
    # def __init__(self):
    #     #self.dataframe = dataframe


    # def time_series(self):
    #     pass
    def map(self, data):
        station_names = []
        station_lons = []
        station_lats = []
        station_lons_end = []
        station_lats_end = []

        file_path = '_01_data_ingestion/data/datasetigs14/coordinates.txt'
        with open(file_path) as f:
            lines = f.readlines()
            line_number = 0
            for line in lines:
                line_number += 1

            for i in range(line_number - 2):
                name = lines[i + 2][0:4]
                lon = lines[i + 2][5:17].strip()
                lat = lines[i + 2][18:27].strip()
                station_names.append(name)
                station_lons.append(float(lon))
                station_lats.append(float(lat))

        fig = plt.figure(num=None, figsize=(12, 8))

        # map1 = Basemap(llcrnrlon=90, llcrnrlat=0, urcrnrlon=115, urcrnrlat=23, epsg=3414)
        # # http://server.arcgisonline.com/arcgis/rest/services
        #
        # map1.arcgisimage(service='World_Shaded_Relief', xpixels=3000, verbose=True)
        # # ESRI_Imagery_World_2D
        #
        # # map1 = Basemap(width=3500000, height=2500000, resolution='h', projection='aea', lat_1=1., lat_2=1, lon_0=105,
        # #                lat_0=12) #aea
        # #
        # #map1.drawcoastlines(linewidth=0.5)
        # #map1.fillcontinents(color='tan', lake_color='lightblue')
        # # draw parallels and meridians.
        # map1.drawparallels(np.arange(-90., 91., 15.), labels=[True, True, False, False], dashes=[2, 2])
        # map1.drawmeridians(np.arange(-180., 181., 15.), labels=[False, False, False, True], dashes=[2, 2])
        # #map1.drawmapboundary(fill_color='lightblue')
        # #map1.drawcountries(linewidth=1, linestyle='solid', color='k')

        map1 = Basemap(width=3500000, height=2500000, resolution='h', projection='aea', lat_1=1., lat_2=1, lon_0=105,
                       lat_0=12)

        map1.drawcoastlines(linewidth=0.5)
        map1.fillcontinents(color='tan', lake_color='lightblue')
        # draw parallels and meridians.
        map1.drawparallels(np.arange(-90., 91., 15.), labels=[True, True, False, False], dashes=[2, 2])
        map1.drawmeridians(np.arange(-180., 181., 15.), labels=[False, False, False, True], dashes=[2, 2])
        map1.drawmapboundary(fill_color='lightblue')
        map1.drawcountries(linewidth=1, linestyle='solid', color='k')

        x, y = map1(station_lons, station_lats)
        map1.plot(x, y, 'bo', markersize=5, color='red')

        # for label, xpt, ypt in zip(station_names, x, y):
        #     plt.text(xpt+10000, ypt+10000, label)

        list_of_keys = list(data['name'].unique())

        # print(data.copy()[data['name'] == list_of_keys[22]])
        for i in range(len(list_of_keys)):
            station = data.copy()[data['name'] == list_of_keys[i]]
            last_data_point = station.iloc[-1]
            station_lons_end.append(last_data_point[3])
            station_lats_end.append(last_data_point[1])

        arrow_scale = 10**7

        for i in range(len(station_lats)):
            x, y = map1(station_lons[i], station_lats[i])

            x2, y2 = map1(station_lons_end[i], station_lats_end[i])

            plt.arrow(x, y, (x2 - x) * arrow_scale, (y2 - y) * arrow_scale, fc="k", ec="k",
                      head_width=40000, head_length=40000)

        scale_x, scale_y = map1(90, 2)
        scale_x2, scale_y2 = map1(90.25, 2)

        plt.arrow(scale_x, scale_y, (scale_x2 - scale_x) * 1/arrow_scale, 0, fc='k', ec='k', head_width=40000,
                  head_length=40000)
        plt.text(scale_x + 140000, scale_y - 20000, '25cm')

        ex_point_x, ex_point_y = map1(90, 7.77)
        ex_point_x2, ex_point_y2 = map1(90, 7.35)

        map1.plot(ex_point_x, ex_point_y, 'bo', markersize=5)
        map1.plot(ex_point_x2, ex_point_y2, 'bo', markersize=5)

        plt.savefig('_04_visualisations/Figures/map_only_stations.pdf')
        plt.show()

    def map_specific_time(self, data):
        station_names = []
        station_lons = []
        station_lats = []
        station_lons_end = []
        station_lats_end = []

        fig = plt.figure(num=None, figsize=(12, 8))
        map1 = Basemap(width=3500000, height=2500000, resolution='h', projection='aea', lat_1=1., lat_2=1, lon_0=105,lat_0=12)

        map1.drawcoastlines(linewidth=0.5)
        map1.fillcontinents(color='tan', lake_color='lightblue')
        # draw parallels and meridians.
        map1.drawparallels(np.arange(-90., 91., 15.), labels=[True, True, False, False], dashes=[2, 2])
        map1.drawmeridians(np.arange(-180., 181., 15.), labels=[False, False, False, True], dashes=[2, 2])
        map1.drawmapboundary(fill_color='lightblue')
        map1.drawcountries(linewidth=1, linestyle='solid', color='k')


        # fig = plt.figure(num=None, figsize=(12, 8))
        #
        # map1 = Basemap(llcrnrlon=90, llcrnrlat=0, urcrnrlon=115, urcrnrlat=23, epsg=3414)
        # # http://server.arcgisonline.com/arcgis/rest/services
        #
        # map1.arcgisimage(service='World_Shaded_Relief', xpixels=3000, verbose=True)
        # # ESRI_Imagery_World_2D
        #
        # # map1 = Basemap(width=3500000, height=2500000, resolution='h', projection='aea', lat_1=1., lat_2=1, lon_0=105,
        # #                lat_0=12) #aea
        # #
        # #map1.drawcoastlines(linewidth=0.5)
        # #map1.fillcontinents(color='tan', lake_color='lightblue')
        # # draw parallels and meridians.
        # map1.drawparallels(np.arange(-90., 91., 15.), labels=[True, True, False, False], dashes=[2, 2])
        # map1.drawmeridians(np.arange(-180., 181., 15.), labels=[False, False, False, True], dashes=[2, 2])
        # #map1.drawmapboundary(fill_color='lightblue')
        # #map1.drawcountries(linewidth=1, linestyle='solid', color='k')


        # for label, xpt, ypt in zip(station_names, x, y):
        #     plt.text(xpt+10000, ypt+10000, label)

        list_of_keys = list(data['name'].unique())

        #print(data.copy()[data['name'] == list_of_keys[14]])

        for i in range(len(list_of_keys)):

            station = data.copy()[data['name'] == list_of_keys[i]]
            line = False
            for j in range(len(station)):
                row = station.iloc[j]

                if j == 0:
                    data_point = station.iloc[0]
                    station_lons.append(data_point[3])
                    station_lats.append(data_point[1])

                if j == 0 and row.name > dt.datetime(2004, 12, 26):
                    data_point = station.iloc[0]
                    station_lons_end.append(data_point[3])
                    station_lats_end.append(data_point[1])
                    break

                if row.name > dt.datetime(2004, 12, 26):
                    # print(row.name, dt.datetime(2004, 12, 26), True)
                    last_data_point = station.iloc[j-1]
                    station_lons_end.append(last_data_point[3])
                    station_lats_end.append(last_data_point[1])
                    break

        x, y = map1(station_lons, station_lats)
        map1.plot(x, y, 'bo', markersize=5)

        arrow_scale = 10**6

        for i in range(len(station_lats)):
            print(station_lons[i] - station_lons_end[i])
            print(station_lats[i] - station_lats_end[i])
            if ((station_lons[i] - station_lons_end[i]) and (station_lats[i] - station_lats_end[i])) == 0.0:

                continue

            else:
                x, y = map1(station_lons[i], station_lats[i])

                x2, y2 = map1(station_lons_end[i], station_lats_end[i])

                plt.arrow(x, y, (x2-x)*arrow_scale, (y2-y)*arrow_scale, fc="k", ec="k",
                          head_width=40000, head_length=40000)

        scale_x, scale_y = map1(90,2)
        scale_x2, scale_y2 = map1(90.0000010,2)

        plt.arrow(scale_x, scale_y, (scale_x2-scale_x)*arrow_scale, 0, fc='k', ec='k',head_width=40000, head_length=40000)
        plt.text(scale_x+140000, scale_y-20000, '10cm')

        ex_point_x, ex_point_y = map1(90,7.77)
        ex_point_x2, ex_point_y2 = map1(90, 7.35)

        # map1.plot(ex_point_x, ex_point_y, 'bo', markersize=5)
        # map1.plot(ex_point_x2, ex_point_y2, 'bo', markersize=5)

        plt.savefig('_04_visualisations/figures/map_pre_2004_update.pdf')

        plt.show()


