import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter
import pandas as pd


'''
This class consists of a function plotting the timeseries (llh) for 1 specific station. Furthermore, the linear parts
within the graph can be approximated by a linear trend line. The slope of the trend line is calculated as well and therefor
the velocity of the station within that linear part is known.
'''

class SimplePlot:
    def __init__(self, dataframe, station, data_list=None,  style='fast'):
        self.dataframe = dataframe
        self.data_list = data_list
        self.station = station
        plt.style.use(style)

    # Input variables for axis are: 'longitude', 'latitude', 'radians' or 'all'
    def timeseries(self, axis):
        # This function plots the trend line within the graph, section can be chosen manually in order to only select linear parts
        def plot_trendline(date, data, period, section, subplot):
            dates = list(mdates.date2num(date))

            dates = [float(np_float) for np_float in dates]

            data = data.values.tolist()

            sliced = slice(*map(lambda x: int(x.strip()) if x.strip() else None, section.split(':')))

            # This is a polynomial fit of degree 1 (linear)
            fit = np.polyfit(dates[sliced], data[sliced], deg=1)
            p = np.poly1d(fit)
            axs[subplot].plot(dates, p(dates), "r--")

            seismic_trend = fit[0] * 365
            print(period, seismic_trend * 1000, 'mm/yr')

        if self.dataframe is not None:
            if axis == 'longitude':
                data = self.dataframe[self.dataframe['name'] == self.station]['lon (deg E) (m)']

            elif axis == 'latitude':
                data = self.dataframe[self.dataframe['name'] == self.station]['lat (deg N) (m)']

            elif axis == 'radians':
                data = self.dataframe[self.dataframe['name'] == self.station]['rad (m)']

            elif axis == 'all':
                data1 = self.dataframe[self.dataframe['name'] == self.station]['lon (deg E) (m)']
                data2 = self.dataframe[self.dataframe['name'] == self.station]['lat (deg N) (m)']
                data3 = self.dataframe[self.dataframe['name'] == self.station]['rad (m)']

                dates = self.dataframe[self.dataframe['name'] == self.station].index

                fig, axs = plt.subplots(3)

                # Setting up the graph, consisting of 3 subplots
                fig.suptitle(self.station)
                axs[0].plot(dates, data1, 's', markersize=1)
                axs[1].plot(dates, data2, 's', markersize=1)
                axs[2].plot(dates, data3, 's', markersize=1)

                plot_trendline(dates, data1, 'all', '0:200', 0)
                plot_trendline(dates, data1, 'all', '425:515', 0)

            else:
                print("Wrong axis")


            if axis =='longitude' or axis =='latitude' or axis =='radians':
                dates = self.dataframe[self.dataframe['name'] == self.station].index
                plt.plot(dates, data, 's', markersize=1,label = self.station)

                trendline_dates = list(mdates.date2num(dates))

                trendline_dates = [float(np_float) for np_float in trendline_dates]

                data = data.values.tolist()

                #Inter-seismic
                fit_inter = np.polyfit(trendline_dates[0:200], data[0:200], deg=1)

                p_inter = np.poly1d(fit_inter)
                plt.plot(trendline_dates, p_inter(trendline_dates), "r--")

                inter_seismic_trend = fit_inter[0]*365
                print('Inter-seismic trend: ',inter_seismic_trend*1000, 'mm/yr')

                #Post-seismic
                fit_post = np.polyfit(trendline_dates[700:800], data[700:800], deg=1)
                p_post = np.poly1d(fit_post)
                plt.plot(trendline_dates, p_post(trendline_dates), "r--")

                post_seismic_trend = fit_post[0]*365
                print('Post-seismic trend: ',post_seismic_trend*1000, 'mm/yr')

        plt.xlabel('Time')
        plt.ylabel(f'{axis}-position')
        plt.ticklabel_format(useOffset=False, axis='y')

        plt.legend()
        plt.show()

'''
This class consists of a function plotting the timeseries (llh) for all stations and saving them as figures. The code 
looks almost the same as above, the only difference is an extra for loop to loop through all stations. Trendlines will 
not be plotted in this class and/or function because we need to manually choose the linear section within the graph. To
see trendlines, use SimplePlot  
'''

class AllDataPlots:
    def __init__(self, dataframe, style='fast'):
        self.dataframe = dataframe

        plt.style.use(style)

    def plot_all_stations(self):
        list_of_keys = list(self.dataframe['name'].unique())

        for i in range(len(list_of_keys)):
            station = self.dataframe.copy()[self.dataframe['name'] == list_of_keys[i]]

            data1 = station['lon (deg E) (m)']
            data2 = station['lat (deg N) (m)']
            data3 = station['rad (m)']

            dates = station.index

            fig, axs = plt.subplots(3, figsize=(5,5))
            fig.tight_layout(pad=2.0)

            if station.index[0] < pd.Timestamp('2004-12-26'):
                axs[0].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)
                axs[1].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)
                axs[2].axvline(pd.Timestamp('2004-12-26'), color='green', alpha=0.3)

                # date = pd.to_datetime('2004-12-26')
                # zero_measure = station.iloc[station.index.get_loc(date, method='ffill')]
                # zero_measure_lon = float(zero_measure[3])
                # zero_measure_lat = float(zero_measure[1])
                # zero_measure_height = float(zero_measure[5])
                #
                # axs[0].plot(dates, (data1 - zero_measure_lon) * 10 ** 7, 's', markersize=0.5)
                # axs[1].plot(dates, (data2 - zero_measure_lat) * 10 ** 7, 's', markersize=0.5)
                # axs[2].plot(dates, (data3 - zero_measure_height) * 10 ** 2, 's', markersize=0.5)
            # else:
            #     axs[0].plot(dates, data1, 's', markersize=0.5)
            #     axs[1].plot(dates, data2, 's', markersize=0.5)
            #     axs[2].plot(dates, data3, 's', markersize=0.5)


            if station.index[0] < pd.Timestamp('2005-03-28'):
                axs[0].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)
                axs[1].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)
                axs[2].axvline(pd.Timestamp('2005-03-28'), color='green', alpha=0.3)
            if station.index[0] < pd.Timestamp('2012-04-11'):
                axs[0].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)
                axs[1].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)
                axs[2].axvline(pd.Timestamp('2012-04-11'), color='green', alpha=0.3)

            if station.index[0] < pd.Timestamp('2004-12-26'):
                date = pd.to_datetime('2004-12-26')
                zero_measure = station.iloc[station.index.get_loc(date, method='ffill')]
                zero_measure_lon = float(zero_measure[3])
                zero_measure_lat = float(zero_measure[1])
                zero_measure_height = float(zero_measure[5])

                axs[0].plot(dates, (data1 - zero_measure_lon) * 10 ** 7, 's', markersize=0.5)
                axs[1].plot(dates, (data2 - zero_measure_lat) * 10 ** 7, 's', markersize=0.5)
                axs[2].plot(dates, (data3 - zero_measure_height) * 10 ** 2, 's', markersize=0.5)

            elif station.index[0] > pd.Timestamp('2004-12-26') and station.index[0] < pd.Timestamp('2005-03-28'):
                date = pd.to_datetime('2005-03-28')
                zero_measure = station.iloc[station.index.get_loc(date, method='ffill')]
                zero_measure_lon = float(zero_measure[3])
                zero_measure_lat = float(zero_measure[1])
                zero_measure_height = float(zero_measure[5])

                axs[0].plot(dates, (data1 - zero_measure_lon) * 10 ** 7, 's', markersize=0.5)
                axs[1].plot(dates, (data2 - zero_measure_lat) * 10 ** 7, 's', markersize=0.5)
                axs[2].plot(dates, (data3 - zero_measure_height) * 10 ** 2, 's', markersize=0.5)

            elif station.index[0] > pd.Timestamp('2005-03-28') and station.index[0] < pd.Timestamp('2012-04-11'):
                date = pd.to_datetime('2012-04-11')
                zero_measure = station.iloc[station.index.get_loc(date, method='ffill')]
                zero_measure_lon = float(zero_measure[3])
                zero_measure_lat = float(zero_measure[1])
                zero_measure_height = float(zero_measure[5])

                axs[0].plot(dates, (data1 - zero_measure_lon) * 10 ** 7, 's', markersize=0.5)
                axs[1].plot(dates, (data2 - zero_measure_lat) * 10 ** 7, 's', markersize=0.5)
                axs[2].plot(dates, (data3 - zero_measure_height) * 10 ** 2, 's', markersize=0.5)

            fig.suptitle(list_of_keys[i])

            plt.xlabel('Time')
            axs[0].set_ylabel('Longitude (cm)')
            axs[1].set_ylabel('Latitude (cm)')
            axs[2].set_ylabel('Height (cm)')

            axs[0].ticklabel_format(useOffset=False, axis='y')
            axs[0].yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
            axs[1].ticklabel_format(useOffset=False, axis='y')
            axs[1].yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
            axs[2].ticklabel_format(useOffset=False, axis='y')
            axs[2].yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

            plt.savefig('_04_visualisations/figures/d14/d14_%s.png' % list_of_keys[i], bbox_inches='tight')

