import numpy as np
import os
import unittest
from _01_data_ingestion.reference_frame_conversion import xyz_to_llh, llh_to_xyz
from _01_data_ingestion.ingestion import Ingestion


class TestDataIngestion(unittest.TestCase):
    def test_reference_frame_conversion(self):
        # just a randomly chosen number such that the random_sample function returns a constant value
        np.random.seed(111000)
        n = 10 ** 3  # The total number of tests to be run

        sample = 5 * 10 ** 6 - 4 * np.random.random_sample(
            (n, 3)) * 10 ** 6  # Yields values between  1 and 9 times 10**6

        # Now some of these values for x and y must be negative. So I make half of them negative
        sample[:, 0][::2] = sample[:, 0][::2] * (-1)
        sample[:, 1][::2] = sample[:, 1][::2] * (-1)

        error_lst = []
        for start_xyz in sample:
            middle_step_llh = xyz_to_llh(start_xyz)
            middle_step_llh = np.array([middle_step_llh[0] * np.pi / 180.,
                                        middle_step_llh[1] * np.pi / 180.,
                                        middle_step_llh[2]])
            error = start_xyz - llh_to_xyz(middle_step_llh)
            squared_error = np.sqrt(np.sum(error ** 2))  # To have an idea of the error in the 3 dimensions
            error_lst.append(float(squared_error))
        if not max(error_lst) < 2*10**-4:
            print('ERROR: error is', max(error_lst))
        self.assertTrue(max(error_lst) < 2 * 10 ** -4)  # This represents a max error of 0.0002 m

    def test_ingestion_08(self):
        def run_08_ingestion_test(data, delete_cache=False, delete_skipped_files=False):
            if delete_cache:
                os.remove('_01_data_ingestion/cache/data_08.pickle')
            if delete_skipped_files:
                os.remove('_01_data_ingestion/cache/data_08_skipped_files.pickle')

            skipped_files = data.ingest_08()
            # Checking that we ran through the right files
            # if len(skipped_files) != 6:
            #     print(skipped_files)
            #     print('ERROR: the length of the skipped files is', len(skipped_files))
            assert len(skipped_files) == 6

            # Checking the total number of stations found
            # if len(data.data_08.keys()) != 82 +1:
            #     print('ERROR: the number of keys is not 82, but', len(data.data_08.keys()))
            assert len(data.data_08['name'].unique()) == 82

        run_08_ingestion_test(Ingestion(debug=True, use_cached=True), delete_cache=False, delete_skipped_files=False)
        run_08_ingestion_test(Ingestion(debug=False, use_cached=True), delete_cache=False, delete_skipped_files=False)

        run_08_ingestion_test(Ingestion(debug=True, use_cached=False), delete_cache=False, delete_skipped_files=False)
        run_08_ingestion_test(Ingestion(debug=False, use_cached=False), delete_cache=False, delete_skipped_files=False)

        run_08_ingestion_test(Ingestion(debug=True, use_cached=True), delete_cache=True, delete_skipped_files=True)
        run_08_ingestion_test(Ingestion(debug=False, use_cached=True), delete_cache=True, delete_skipped_files=True)

        run_08_ingestion_test(Ingestion(debug=True, use_cached=True), delete_cache=False, delete_skipped_files=True)
        run_08_ingestion_test(Ingestion(debug=False, use_cached=True), delete_cache=False, delete_skipped_files=True)

    def test_ingestion_14(self):
        def run_14_ingestion_test(data):
            skipped_files = data.ingest_14()
            # Checking that we ran through the right files
            # if len(skipped_files) != 3:
            #     print('ERROR: the length of the skipped files is', len(skipped_files))
            assert len(skipped_files) == 3

            # Checking the total number of stations found
            number_of_sites = len(data.data_14['name'].unique())
            if number_of_sites != 30:
                print('ERROR: the number of keys is not 30, but', number_of_sites)
            assert number_of_sites == 30

            # Checking that the dataframe has the right number of entries
            assert data.data_14.shape == (6494, 8)

        # All combinations of cached and debugging
        run_14_ingestion_test(Ingestion(debug=True, use_cached=False))
        run_14_ingestion_test(Ingestion(debug=False, use_cached=False))
        run_14_ingestion_test(Ingestion(debug=True, use_cached=True))
        run_14_ingestion_test(Ingestion(debug=False, use_cached=True))

        # Now try to run with cached while there is no cached pickle
        os.remove('_01_data_ingestion/cache/data_14.pickle')
        os.remove('_01_data_ingestion/cache/data_14_skipped_files.pickle')
        run_14_ingestion_test(Ingestion(debug=True, use_cached=True))
        os.remove('_01_data_ingestion/cache/data_14.pickle')
        os.remove('_01_data_ingestion/cache/data_14_skipped_files.pickle')
        run_14_ingestion_test(Ingestion(debug=False, use_cached=True))

        # Now, we do the exact same thing again, except with the pickle present, but not the cached files pickle
        os.remove('_01_data_ingestion/cache/data_14_skipped_files.pickle')
        run_14_ingestion_test(Ingestion(debug=True, use_cached=True))
        os.remove('_01_data_ingestion/cache/data_14_skipped_files.pickle')
        run_14_ingestion_test(Ingestion(debug=False, use_cached=True))
