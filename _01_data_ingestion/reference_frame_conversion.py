import numpy as np
a = 6378137.0  # m based on WGS84
b = 6356752.314245  # m based on WGS84


def first_eccentricity(equatorial_radius, polar_radius):
    return np.sqrt((equatorial_radius ** 2 - polar_radius ** 2) / equatorial_radius ** 2)


e = first_eccentricity(a, b)

#
# # Function for converting long lat height back to xyz from:
# # https://www.linz.govt.nz/data/geodetic-system/coordinate-conversion/geodetic-datum-conversions/equations-used-datum
# # Error when transforming back: 4*10**-10 - 9*10**-10
# # input([long, lat, height])
def llh_to_xyz(llh):
    v = a / np.sqrt(1 - (e ** 2 * (np.sin(llh[1]) ** 2)))
    x = (v + llh[2]) * np.cos(llh[1]) * np.cos(llh[0])
    y = (v + llh[2]) * np.cos(llh[1]) * np.sin(llh[0])
    z = (v * (1 - e ** 2) + llh[2]) * np.sin(llh[1])
    return np.array([x, y, z])


# xyz to llh function based on: Article: Transforming Geocentric Cartesian Coordinates to Geodetic Coordinates
# by a New Initial Value Calculation Paradigm
def xyz_to_llh(xyz):
    p_g = np.sqrt(xyz[0] ** 2 + xyz[1] ** 2)

    k = np.sqrt((p_g ** 2 / a ** 2) + (xyz[2] ** 2 / b ** 2))

    e_c = np.sqrt(1 - e ** 2)

    t_0_nom = e_c * (k ** 2 * a ** 2 + (k - 1) * (p_g ** 2 + xyz[2] ** 2)) * xyz[2]
    t_0_den = (k ** 2 * b ** 2 + (k - 1) * (p_g ** 2 + xyz[2] ** 2)) * p_g + 10. ** -6

    t_0 = t_0_nom / t_0_den

    c = 1 / np.sqrt(e_c ** 2 + t_0 ** 2)

    result = np.zeros(3, dtype=np.float64)

    result[2] = (e_c * p_g + xyz[2] * t_0 - b * np.sqrt(1 + t_0 ** 2)) * c  # h

    n = (np.sqrt(a ** 2 - e ** 2 * (p_g - e_c * result[2] * c) ** 2)) / e_c

    # phi
    result[1] = (np.arctan(((n + result[2]) * xyz[2]) / ((n * e_c ** 2 + result[2]) * p_g + 10. ** -6))) * 180. / np.pi

    result[0] = 2. * np.arctan(xyz[1] / (xyz[0] + np.sqrt(xyz[0] ** 2 + xyz[1] ** 2))) * 180. / np.pi  # lambda

    return result

