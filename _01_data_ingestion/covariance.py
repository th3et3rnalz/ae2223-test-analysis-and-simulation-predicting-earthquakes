import numpy as np
import pandas as pd
import os
import pickle
from _01_data_ingestion.reference_frame_conversion import xyz_to_llh
'''
In this class the conversion of xyz coordinates to llh will be done with the use of a conversion method in a separate
file. The dataframe will be constructed the same as the d14 dataframe in order to merge the dataframes in a later 
stage. Furthermore, caching is being used to reduce the computation time. 
'''

class Covariance:
    def __init__(self, debug=True, use_cached=True):
        self.new_data_pd = {}
        self.debug = debug
        self.use_cached = use_cached

    # This function does the multiplication with the covariance matrix and returns the adjusted coordinates
    def cov(self, x, y, z, covmat):
        xyz = np.array([[x], [y], [z]])
        newxyz = np.dot(covmat, xyz)
        lstxyz = []
        lstxyz.append(newxyz[0][0])
        lstxyz.append(newxyz[1][0])
        lstxyz.append(newxyz[2][0])

        return newxyz

    ''' This function does the actual conversion. First the program checks if a pickle (cached file) has been saved
    already, if so, it will use that file to make the computation time shorter. If the user wants to debug or the pickle
    file is not found, the conversion will run completely.
    This function returns a Pandas DataFrame.
    '''
    def transform_format_from_d08_to_d14(self, data):

        if self.use_cached:
            if self.debug:
                print('CACHING ON')
            files = os.listdir('_01_data_ingestion/cache')
            if 'conversion.pickle' in files:
                if self.debug:
                    print('USING CACHED PICKLE')
                f = open('_01_data_ingestion/cache/conversion.pickle', 'rb')
                self.new_data_pd = pickle.load(f)
                f.close()
                return self.new_data_pd
            else:
                if self.debug:
                    print('NOT USING CACHED PICKLE; NOT FOUND')

        list_of_keys = list(data['name'].unique())
        all_data = []
        # print(list_of_keys)
        for i in range(len(list_of_keys)):
            new_data = []
            station = data.copy()[data['name'] == list_of_keys[i]]

            # Retreiving the data from the original d08 set.
            for row in station.itertuples(index=True):
                time0 = row.Index
                time = pd.to_datetime(time0, format='%Y-%m-%d')
                name = row.name
                x = row.x
                y = row.y
                z = row.z
                x_uncertainty = row.x_uncertainty
                y_uncertainty = row.y_uncertainty
                z_uncertainty = row.z_uncertainty
                yx_correlation = row.yx_correlation
                zx_correlation = row.zx_correlation
                zy_correlation = row.zy_correlation

                # Using the xyz to llh function to convert the coordinates
                llh = xyz_to_llh([x, y, z])

                # Defining the covariance matrix which will be used to convert the sigmas in xyz direction
                covmat = np.array([[1, yx_correlation, zx_correlation],
                                   [yx_correlation, 1, zy_correlation],
                                   [zx_correlation, zy_correlation, 1]])

                standard_dev = self.cov(x_uncertainty, y_uncertainty, z_uncertainty, covmat)

                entries = [time, name, llh[1], abs(standard_dev[1][0] * 100), llh[0], abs(standard_dev[0][0] * 100),
                           llh[2], abs(standard_dev[2][0] * 100), 'country']

                new_data.append(entries)

            for i in range(len(new_data)):
                all_data.append(new_data[i])

        df = pd.DataFrame(all_data, columns=['datetime',
                                             'name',
                                             'lat (deg N) (m)',
                                             'lat_dev (cm)',
                                             'lon (deg E) (m)',
                                             'lon_dev (cm)',
                                             'rad (m)',
                                             'rad_dev (cm)',
                                             'country'])

        self.new_data_pd = df.set_index('datetime')
        self.new_data_pd.sort_values(by='datetime', inplace=True, ascending=True)

        # A new copy of the dataframe will be saved to the pickle file
        outfile = open('_01_data_ingestion/cache/conversion.pickle', 'wb')
        pickle.dump(self.new_data_pd, outfile)
        outfile.close()

        return self.new_data_pd
