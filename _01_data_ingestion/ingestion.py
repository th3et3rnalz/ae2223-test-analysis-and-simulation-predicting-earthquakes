import pandas as pd
import os
import datetime as dt
import pickle


class Ingestion:
    def __init__(self, debug=False, use_cached=True):
        self.files_08 = sorted(os.listdir('_01_data_ingestion/data/datasetigs08/Daily'))
        self.files_14 = sorted(os.listdir('_01_data_ingestion/data/datasetigs14'))
        self.data_08 = {}
        self.data_14 = {}
        self.data_14_ref = pd.DataFrame()
        self.debug = debug
        self.use_cached = use_cached
        self.month_dict = {'JAN': 1, 'FEB': 2, 'MAR': 3, 'APR': 4, 'MAY': 5, 'JUN': 6,
                           'JUL': 7, 'AUG': 8, 'SEP': 9, 'OCT': 10, 'NOV': 11, 'DEC': 12}

    def ingest_14(self):

        """
        This function ingests the files from the datasetigs14 folder, in that format.
        It stores this data in a class variable named data_14. Hence, to access the data,
        one must call 'my_instance_name.data_14' where my_instance_name is the name of your
        instance of this class. The data returned in this way is a dict with an entry for
        every station, and every station is a pandas dataframe.

        :return: list of skipped files, this is solely used for unit testing, of no use for
        the end user.
        """
        if not isinstance(self.data_14, dict):
            return False

        if self.use_cached:
            if self.debug:
                print('CACHING ON')
            files = os.listdir('_01_data_ingestion/cache')
            if 'data_14.pickle' in files:
                if self.debug:
                    print('USING CACHED PICKLE')
                f = open('_01_data_ingestion/cache/data_14.pickle', 'rb')
                self.data_14 = pickle.load(f)
                f.close()

                # For some reason, when we unpickle, the names are gone, so we must add those again:
                for key in self.data_14.keys():
                    if key != 'type':
                        self.data_14[key].name = key
                if 'data_14_skipped_files.pickle' in files:
                    skipped = open('_01_data_ingestion/cache/data_14_skipped_files.pickle', 'rb')
                    skipped_list = pickle.load(skipped)
                    skipped.close()
                    return skipped_list
                else:
                    self.data_14 = {}
                    if self.debug:
                        print("FOUND DATA_14 PICKLE, BUT NOT THE SKIPPED FILES PICKLE")
                        print("HENCE, PROCEEDING WITH NORMAL INGESTION")
            else:
                if self.debug:
                    print('CACHED PICKLE NOT FOUND')

        skipped = []
        for file in self.files_14:
            if file[-4:len(file)] == '.txt' or file[-4:] == '.zip':
                if self.debug:
                    print(file, 'skipped')
                skipped.append(file)
                continue

            f = open("_01_data_ingestion/data/datasetigs14/" + file, 'r', encoding='latin-1')
            file_data = f.readlines()
            f.close()
            param = len(file_data)
            name = file[0:4]

            if file[-3:] == 'lat':
                parsed_data = []
                for n in range(int(param)):
                    data_coordinates = self.parse_14_coordinate(n, file_data)
                    if self.debug:
                        print(data_coordinates)
                    # We don't need the date of the measurement in every entry
                    # data_coordinates[1] = float(data_coordinates[1])*10**(-2)
                    parsed_data.append([data_coordinates[0]] + [name] + data_coordinates[1:])
                self.data_14[name] = pd.DataFrame(parsed_data, columns=['datetime',
                                                                        'name',
                                                                        'lat_weekly_shift',  # cm
                                                                        'lat_dev'])  # cm
            else:
                parsed_data_weekly = []
                parsed_data_dev = []
                for n in range(int(param)):
                    data_coordinates = self.parse_14_coordinate(n, file_data)
                    if self.debug:
                        print(data_coordinates)
                    # We don't need the date of the measurement in every entry
                    parsed_data_weekly.append(data_coordinates[1])
                    parsed_data_dev.append(data_coordinates[2])

                if file[-3:] == 'lon':
                    self.data_14[name]['lon_weekly_shift'] = parsed_data_weekly
                    self.data_14[name]['lon_dev'] = parsed_data_dev
                if file[-3:] == 'rad':
                    self.data_14[name]['rad_weekly_shift'] = parsed_data_weekly
                    self.data_14[name]['rad_dev'] = parsed_data_dev
                    self.data_14[name]['country'] = 'TBD'
                else:
                    Exception(f"WHAT THE HELL, filename:{file}")

        # Now we are going to merge all of these to each other, to be consistent with the 08 structure

        self.data_14 = pd.concat([self.data_14[key] for key in list(self.data_14.keys())])
        self.data_14.sort_index()

        '''merging the reference coordinates from 2004'''
        # Converting the two data frames created in ingest_14 and ingest _14_ref to numpy arrays
        arr_offset = self.data_14.to_numpy()
        arr_ref = self.ingest_14_ref_coord().to_numpy()

        # for each station(which in this case is equivalent to a row) in the data frame for reference coordinates form
        # 2004 check if the same name in the offset data frame. If the names match, add the reference coordinates to
        # the offset, else continue
        for row_ref in range(self.data_14_ref.shape[0]):
            name_1 = arr_ref[row_ref][0]
            for row_offset in range(self.data_14.shape[0]):
                name_2 = arr_offset[row_offset][1]
                if name_1 == name_2:
                    arr_offset[row_offset][2] = arr_offset[row_offset][2] * 10 ** (-7) + arr_ref[row_ref][2]
                    arr_offset[row_offset][4] = arr_offset[row_offset][4] * 10 ** (-7) + arr_ref[row_ref][1]
                    arr_offset[row_offset][6] = arr_offset[row_offset][6] * 10 ** (-2) + arr_ref[row_ref][3]
                    arr_offset[row_offset][8] = arr_ref[row_ref][4]
                else:
                    continue

        # converting the numpy array back to a panda data frame
        df = pd.DataFrame(arr_offset, columns=['datetime',
                                               'name',
                                               'lat (deg N) (m)',
                                               'lat_dev (cm)',
                                               'lon (deg E) (m)',
                                               'lon_dev (cm)',
                                               'rad (m)',
                                               'rad_dev (cm)',
                                               'country'])

        self.data_14 = df.set_index('datetime')
        # This is the data frame we want. However, i'm not sure what to do with the pickle, cache check right now.
        # this is the only one we need so we can just take out d14 = data.data_14 and
        #  d14r = data.data_14_ref from the main. I'm not sure how to make this function return just df

        outfile_skipped_files = open('_01_data_ingestion/cache/data_14_skipped_files.pickle', 'wb')
        pickle.dump(skipped, outfile_skipped_files)
        outfile_skipped_files.close()

        outfile = open('_01_data_ingestion/cache/data_14.pickle', 'wb')
        pickle.dump(self.data_14, outfile)
        outfile.close()

        return skipped

    def parse_14_coordinate(self, base_line_number, data):
        weekly_avg_position_shit = float(data[base_line_number][13:21])
        std_deviation = float(data[base_line_number][25:32])
        date = data[base_line_number][-8:-1]
        date = self.make_date_string_into_datetime(date)
        if self.debug:
            print('1.', date)
            print('2.', weekly_avg_position_shit)
            print('3.', std_deviation)
        return [date, weekly_avg_position_shit, std_deviation]

    def ingest_14_ref_coord(self):
        for file in self.files_14:
            if file[-4:len(file)] == '.lat' or file[-4:] == '.lon' or file[-4:] == '.rad':
                continue

            f = open("_01_data_ingestion/data/datasetigs14/" + file, 'r', encoding='latin-1')
            file_data = f.readlines()
            f.close()
            param = len(file_data)
            date = self.make_date_string_into_datetime('04DEC25')

            if file[-3:] == 'txt':
                parsed_data = []
                for n in range(2, int(param)):
                    data_coordinates = self.parse_14_ref_coord(n, file_data)
                    parsed_data.append([date] + data_coordinates)
                self.data_14_ref = pd.DataFrame(parsed_data, columns=['datetime',
                                                                      'name',
                                                                      'base_lon (deg E)',
                                                                      'base_lat (deg N)',
                                                                      'base_rad (m)',
                                                                      'country'])
                self.data_14_ref.set_index('datetime', inplace=True)
        return self.data_14_ref

    @staticmethod
    def parse_14_ref_coord(base_line_number, data):
        name = data[base_line_number][0:4]
        longitude = float(data[base_line_number][5:18])
        latitude = float(data[base_line_number][19:30])
        height = float(data[base_line_number][31:40])
        location = data[base_line_number][41:49]
        return [name, longitude, latitude, height, location]

    def ingest_08(self):
        """
        This function ingests the files from the datasetigs08 folder, in that format.
        It stores this data in a class variable named data_08. Hence, to access the data,
        one must call 'my_instance_name.data_08' where my_instance_name is the name of your
        instance of this class. The data returned in this way is a dict with an entry for
        every station, and every station is a pandas dataframe.

        :return: list of skipped files, this is solely used for unit testing, of no use for
        the end user.
        """
        if self.use_cached:
            files = os.listdir('_01_data_ingestion/cache')
            if self.debug:
                print('CACHING ON')
            if 'data_08.pickle' in files:
                if self.debug:
                    print('FOUND CACHED PICKLE')
                f = open('_01_data_ingestion/cache/data_08.pickle', 'rb')
                self.data_08 = pickle.load(f)
                f.close()

                # For some reason, when we unpickle, the names are gone, so we must add those again:
                for key in self.data_08.keys():
                    if key != 'type':
                        self.data_08[key].name = key
                if 'data_08_skipped_files.pickle' in files:
                    skipped = open('_01_data_ingestion/cache/data_08_skipped_files.pickle', 'rb')
                    skipped_list = pickle.load(skipped)
                    skipped.close()
                    return skipped_list
                else:
                    self.data_08 = {}
                    if self.debug:
                        print("FOUND DATA_08 PICKLE, BUT NOT THE SKIPPED FILES PICKLE")
                        print("HENCE, PROCEEDING WITH NORMAL INGESTION")
            else:
                if self.debug:
                    print('CACHED PICKLE NOT FOUND')

        skipped = []

        if self.debug and self.use_cached:
            print('NORMAL DATA INGESTION')
        # This will import the files in the format
        all_data = []
        for file in self.files_08:
            # Now we make sure we only try to import the right files
            if file[0] != 'P' or file[-3:] == 'neu':
                if self.debug:
                    print(file, 'skipped')
                skipped.append(file)
                continue

            f = open("_01_data_ingestion/data/datasetigs08/Daily/" + file, 'r')
            file_data = f.readlines()
            f.close()

            param = int(file_data[0][1:5])
            date = self.make_date_string_into_datetime(file_data[0][20:-2])

            # First we must know how many parameters there are:
            if self.debug:
                print('file:', file, '\nFirst Line', file_data[0])

            coordinates = file_data[1:param + 1]
            correlations = file_data[1 + param:]

            for n in range(int(param / 3)):
                line_number = n * 3
                data_coordinates = self.parse_08_coordinate(line_number, coordinates)
                data_correlation = self.parse_08_uncertainty(line_number, correlations)
                all_data.append([date] + data_coordinates + data_correlation)

        self.data_08 = pd.DataFrame(all_data, columns=['datetime',
                                                       'name',
                                                       'x',
                                                       'y',
                                                       'z',
                                                       'x_uncertainty',
                                                       'y_uncertainty',
                                                       'z_uncertainty',
                                                       'yx_correlation',
                                                       'zx_correlation',
                                                       'zy_correlation'])
        self.data_08.set_index('datetime', inplace=True)
        self.data_08.sort_index()

        outfile_skipped_files = open('_01_data_ingestion/cache/data_08_skipped_files.pickle', 'wb')
        pickle.dump(skipped, outfile_skipped_files)
        outfile_skipped_files.close()

        outfile = open('_01_data_ingestion/cache/data_08.pickle', 'wb')
        pickle.dump(self.data_08, outfile)
        outfile.close()

        return skipped

    @staticmethod
    def parse_08_coordinate(base_line_number, data):
        return [data[base_line_number][7:11],
                float(data[base_line_number + 0][25:-28]),
                float(data[base_line_number + 1][25:-28]),
                float(data[base_line_number + 2][25:-28]),
                float(data[base_line_number + 0][53:-1]),
                float(data[base_line_number + 1][53:-1]),
                float(data[base_line_number + 2][53:-1])
                ]

    @staticmethod
    def parse_08_uncertainty(base_line_number, data):
        return [float(data[base_line_number + 0][12:]),
                float(data[base_line_number + 1][12:]),
                float(data[base_line_number + 2][12:])]

    def make_date_string_into_datetime(self, string):
        if int(string[0:2]) < 50:  # Oh no, this program won't give correct results after 2050
            year = int('20' + string[0:2])
        else:
            year = int('19' + string[0:2])
        month = string[2:5]
        day = int(string[5:7])
        month = self.month_dict[month]
        return dt.datetime(year, month, day)
