from _01_data_ingestion.ingestion import Ingestion
from _01_data_ingestion.covariance import Covariance
from pandas import concat, read_csv, set_option
import os

set_option('display.max_rows', 500)
set_option('display.max_columns', 500)
set_option('display.width', 1000)


def get_d14():
    if 'entire_dataset.csv' not in os.listdir('_01_data_ingestion/cache'):
        data = Ingestion(debug=False, use_cached=False)
        cov = Covariance(debug=False, use_cached=False)

        data.ingest_08()
        data.ingest_14()

        d08 = data.data_08
        d14 = data.data_14

        d08_in_d14_format = cov.transform_format_from_d08_to_d14(d08)

        d14 = concat([d14, d08_in_d14_format])
        d14.sort_values(by=['name', 'datetime'], inplace=True)
        d14.to_csv('_01_data_ingestion/cache/entire_dataset.csv')
    else:
        d14 = read_csv('_01_data_ingestion/cache/entire_dataset.csv', parse_dates=['datetime'])
        d14.set_index('datetime', inplace=True)
    return d14


def get_initial_d14():
    data = Ingestion(debug=False, use_cached=False)
    data.ingest_14()
    d14 = data.data_14

    return d14


def get_initial_d08():
    data = Ingestion(debug=False, use_cached=False)
    cov = Covariance(debug=False, use_cached=False)
    data.ingest_08()
    d08 = data.data_08
    d08_in_d14_format = cov.transform_format_from_d08_to_d14(d08)

    return d08_in_d14_format
