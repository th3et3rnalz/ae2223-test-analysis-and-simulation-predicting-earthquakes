from _01_data_ingestion import get_d14, get_initial_d14, get_initial_d08
from _02_error_analysis import clean_d14, test_median, get_cached_outlier_m1
from _03_data_analysis import DFF, clean_fourier, TrainAndPredictRNN, EP
# from _04_visualisations import plot_all


# --------------------------------------------------------------------------------------------------------------------
#     INGESTION AND CLEANING     INGESTION AND CLEANING     INGESTION AND CLEANING     INGESTION AND CLEANING
# --------------------------------------------------------------------------------------------------------------------
# d08 = get_initial_d08()
d14_single = get_initial_d14()
d14 = get_d14()

# ------------------------------
# Data is all in the same format
# We can now start cleaning it
# ------------------------------

# df_no_outliers, df_earthquakes = test_median(d14)
# print(df_no_outliers)

#
# fully_cleaned_data = clean_d14(d14_single)
# # print(fully_cleaned_data)
# without_seasonal = clean_fourier(fully_cleaned_data)
# # df_no_outliers, df_earthquakes = test_median(d14)
# print(print(df_no_outliers))

fully_cleaned_data = clean_d14(d14_single)
without_seasonal = clean_fourier(fully_cleaned_data)
# print(without_seasonal)

# --------------------------------------------------------------------------------------------------------------------
#    MODELING   MODELING   MODELING   MODELING   MODELING   MODELING   MODELING   MODELING   MODELING   MODELING
# --------------------------------------------------------------------------------------------------------------------
df_no_outliers, series_earthquakes = get_cached_outlier_m1(d14)

ATerribleRNN = TrainAndPredictRNN(df_no_outliers, series_earthquakes)
ATerribleRNN.create_and_train_model()
ATerribleRNN.make_predictions(print_them=True)

function_fits = DFF.fit_functions(without_seasonal, normal_exceptions=("CPNT", "CUSV"),
                                  use_all_earthquakes=("ARAU", "LGKW", "PHUK", "UUMK"), plotting=False)
# TrainAndPredictRNN(cleaned)
Predictor = EP(function_fits.time_series, function_fits.regressors)
# extrapolated = Predictor.extrapolate_data(use_cached=True)
# # Predictor.plot_extrapolation(extrapolated_PHUK, stations=["PHUK"])
Predictor.detect_quakes_rootfinding(offset_analysis=("CPNT", "CUSV", "RTSD"))


# --------------------------------------------------------------------------------------------------------------------
#    VISUALIZATIONS    VISUALIZATIONS    VISUALIZATIONS    VISUALIZATIONS    VISUALIZATIONS    VISUALIZATIONS
# --------------------------------------------------------------------------------------------------------------------

# plot_all(d14)
# plot_all(d14_single)



