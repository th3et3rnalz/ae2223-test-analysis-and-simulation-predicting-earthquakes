This is a Github repository for the Test Analysis and Simulation project.

We will use this repository as version control of the software 
that will be written for the project.


Coding conventions follow below.

# General
- Please place a small docstring at the top of your script explaining at least the basic purpose of it. This makes it easier for others to read it.
- Use comments. You don't have to describe every little thing but keep the general procedure clear.
- If you are using another version that Python 3.6.x or you have more than one version of Python installed, please let me know as some adjustments may have to be made.


# Getting started



# Required packages:
Please make sure you have the following packages:
- Pandas
- Numpy
- Matplotlib

# Import statements:
The following import conventions should be followed:
- For Numpy: import numpy as np
- For Matplotlib: import matplotlib.pyplot as plt
- For PyGame: import pygame as pg
- For Tkinter: from tkinter import *
- For Pandas: import pandas as pd
- For threading (seeing as we most likely won't need anything else): from threading import Thread

Important note: please do not use the math module if Numpy is already included into the script. It contains all the same functions that math has. If you do want math in there, do "from math import *".

# Function and Class Definitions
- For functions, use lowercase for the first letter of the name and only capitalize later letters if they are the first one of another word in the function definition.

E.g.: def  findCornerPoints()

- For classes, use uppercase for the first letter of the name any later letters if they are the first one of another word in the class definition.

E.g.: class AnimatedTexture()

- If the contents of your class have to be printed out at any point in the program, consider using the __str__() method inside the class.

E.g.:
<pre>
class Test():
	def __init(self,a):
		self.a = a
	def __str__(self):
		toreturn = "This is a Test class with a = "+str(self.a)
		return toreturn
</pre>
- If a method in a class feels like it should be a property but you can't make it an internal variable, make it a property using the @property decorator.

# Plotting
-	Please always use different marks and different colours for each dataset if they are plotted in the same graph. The report needs to be readable in grayscale and in colour.
-	Assign labels to your data when you plot it and don’t forget the legend.
-	When you let matplotlib save the figure, please use pdf format (It retains its quality if you scale it up in LaTeX).
-	Don’t put a graph title on the graph, that has to be done in LaTeX captions (technical and scientific writing conventions).

Important note about axis titles and legend labels: you can use  LaTeX math script in matplotlib and it will format exactly the same. Just encapsulate it in $$.
E.g.: $C_{L_\alpha}$ will work just fine and give you what you would expect.

