from keras.models import Sequential
from keras.layers import LSTM, Dense
from sklearn.preprocessing import MinMaxScaler
import numpy as np


# Bundling some default RNN behaviour into a custom class to facilitate usage
class RNN:
    def __init__(self, nr_of_layers, nr_of_neurons, lookback, epochs=100):

        self.verbose = 1  # Show progress bar while training.
        self.epochs = epochs

        self.LSTM_neuroncount = nr_of_neurons
        self.LSTM_layercount = nr_of_layers
        self.lookback = lookback  # the number of previous inputs the network must take into account.

        self.scaler = MinMaxScaler(feature_range=(0, 1))
        self.model = Sequential()

        for i in range(nr_of_layers):
            # We use tanh activation as it is often used for LSTM's
            self.model.add(LSTM(nr_of_neurons, input_shape=(1, lookback)))

        # Add an output neuron (single value) to output the displacement.
        self.model.add(Dense(1))

        # The adam optimizer is an efficient variant of gradient descent
        self.model.compile(loss="mean_squared_error", optimizer="adam")

    def prepare_data(self, data):
        assert len(data) % self.lookback == 0

        dataset = []
        for i in range(len(data) // self.lookback):
            dataset.append(data[i * self.lookback: (i + 1) * self.lookback])
        return np.array(dataset, dtype=np.float64)

    # For the data, please make sure there are no gaps. Linearly interpolate for gaps before feeding it into the network
    def train(self, data):
        prepped_data = self.prepare_data(data)
        scaled_data = self.scaler.fit_transform(prepped_data)
        # Data is of the form X1, X2, ... Xn, Y according to lookback size
        X = scaled_data[:, :-1]
        Y = scaled_data[:, -1]

        # Put data shape as required
        X = np.reshape(X, (X.shape[0], X.shape[1], 1))

        self.model.fit(X, Y, epochs=self.epochs, verbose=self.verbose)

    def predict(self, x):
        x = np.asarray(x, dtype=np.float64)

        # Not sure about this following line, leaving it in for now
        x = self.prepare_data(x)

        x = self.scaler.transform(x)
        x = np.reshape(x, (x.shape[0], x.shape[1], 1))

        prediction = self.model.predict(x)
        prediction = self.scaler.inverse_transform(prediction)

        return prediction
