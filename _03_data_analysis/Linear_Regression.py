""""
Linear Regression tools for use in time series curve fitting.
Made by: Liam Cools
For: Test Analysis and Simulation

Regressor functionality is defined using a profile object in order to simplify the creation of multiple regressor
classes with identical functionality for different time series.

Usage is as follows:
Regression settings such as features, cost function and optimization function are defined in the Regression Profile
objects.

Regression settings connected to the data are defined during creation of the Regressor class itself. This includes
settings such as: regularization, use of normal equation and feature scaling.

"""
import numpy as np
import functools
import matplotlib.pyplot as plt # Used only for debugging by plotting the learning curve.


# Optimization functions
def batch_gradient_descent(alpha):
    def batch_gradient_descent(alpha, regressor_object, num_iters=400):

        X = regressor_object.X
        y = regressor_object.y
        theta = regressor_object.theta.copy()
        lambd = regressor_object.lambd


        for iter in range(num_iters):
            if regressor_object.debug:
                regressor_object.track_optmization(iter,theta)
            gradients = regressor_object.profile.dJ(X,theta,y,lambd)
            theta -= alpha * gradients

        return theta.copy()

    return functools.partial(batch_gradient_descent, alpha)


def mini_batch_gradient_descent(alpha, b):
    def mini_batch_gradient_descent(alpha, b, regressor_object, num_iters=100):
        X = regressor_object.X
        m, n = X.shape
        y = regressor_object.y
        theta = regressor_object.theta.copy()
        lambd = regressor_object.lambd

        for iter in range(num_iters):
            for i in range(int(m//b)):
                curit = iter*b + i
                if regressor_object.debug:
                    regressor_object.track_optimization(curit,theta)
                si = i*b
                Xtemp = X[si:si+b]
                ytemp = y[si:si+b]

                gradients = regressor_object.profile.dJ(Xtemp,theta,ytemp,lambd)
                theta -= alpha * gradients

        return theta.copy()

    return functools.partial(mini_batch_gradient_descent, alpha, b)

def stochastic_gradient_descent(alpha):
    def stochastic_gradient_descent(alpha, regressor_object, num_iters=10):
        X = regressor_object.X.copy()
        m,n = X.shape
        y = regressor_object.y.copy()
        theta = regressor_object.theta.copy()
        lambd = regressor_object.lambd

        # Randomly shuffle X and corresponding y
        shuffle_obj = list(zip(X,y))
        np.random.shuffle(shuffle_obj)
        X,y = zip(*shuffle_obj) # unzip them again

        for iter in range(num_iters):
            for i, ex in enumerate(X):
                curit = iter*m+i
                if regressor_object.debug and curit%1000 == 0:
                    regressor_object.track_optimization(curit,theta)

                gradients = regressor_object.profile.dJ(ex.reshape(1,-1), theta, y[i], lambd)
                theta -= alpha*gradients

        return theta.copy()

    return functools.partial(stochastic_gradient_descent, alpha)


# Feature functions
# Mostly for consistency-sake, we don't actually need to define all of these
def sqrt(x):
    return [np.sqrt(x)]


def sin(x):
    return [np.sin(x)]

def sin_alt(multiplier, sumterm):
    return functools.partial(lambda mult, sumt, x: [np.sin(mult * x + sumt)], multiplier, sumterm)


def cos(x):
    return [np.cos(x)]

def cos_alt(multiplier, sumterm):
    return functools.partial(lambda mult, sumt, x: [np.cos(mult * x + sumt)], multiplier, sumterm)


def log(x):
    return [np.log(x)]


def exp(x):
    return [np.exp(x)]

def log_alt(multiplier, sumterm):
    return functools.partial(lambda mult, sumt, x: [np.log( x * mult + sumt)], multiplier, sumterm)

def exp_alt(multiplier, sumterm):
    return functools.partial(lambda mult, sumt, x: [np.exp( x * mult + sumt)], multiplier, sumterm)

def poly(n):
    return functools.partial(lambda n, x: [x ** n], n)


def poly_upto_n(n):
    return functools.partial(lambda n, x: [x ** i for i in range(2, n + 1)], n)

def product_terms(x):
    toreturn = [x[i] * x[j] for i in range(len(x)) for j in range(i + 1, len(x))]
    return toreturn


# Cost functions
def least_squares(X, theta, y, lambd):
    m, n = X.shape
    toreturn = 0.5 / m * np.sum(np.square(X @ theta - y)) + 0.5 / m * lambd * np.sum(theta[1:] ** 2)

    return toreturn


def derivative_least_squares(X, theta, y, lambd):
    m, n = X.shape

    regularization_terms = theta.copy()
    regularization_terms[0] = 0.

    toreturn = (1 / m * np.sum(((X @ theta - y) * X).T, 1) \
                + (lambd / m * np.sum(regularization_terms, 1))).reshape(n, 1)
    toreturn = np.asarray(toreturn,dtype=np.float64)

    return toreturn.copy()

# Regressor Profile class, for easily constructing regression profiles
class Regression_Profile:
    def __init__(self, cost_function, cost_function_derivative, feature_functions, optimization_function):

        """
        :param cost_function: the cost function to use, currently only least squares is implemented
        :param cost_function_derivative: derivative of the cost function
        :param feature_functions: the types of functions and their optional parameters that make up the final regression
                                  function
        :param optimization_function: the type of optimization function to use (batch GD, stochastic GD, mini-batch GD)
        """

        self.J = cost_function
        self.dJ = cost_function_derivative
        self.features = feature_functions
        self.optimize = optimization_function


# Regressor class, this is where the actual magic happens
class Regressor:
    def __init__(self, Regressor_Settings, use_normal_equation=False,\
                 use_feature_scaling=True, regularization_parameter=0, debug=False):

        """
        :param Regressor_Settings: a settings object containing information about the type of functions that make up
                                   the final regression function
        :param use_normal_equation: this will fit the data in one matrix operation, if the data matrix is invertible
        :param use_feature_scaling: this puts all features on a similar scale. It greatly improves fit accuracy
        :param regularization_parameter: if data is being over-fitted, regularization can be used to smooth the function
        :param debug: show learning curve after fitting
        """

        self.profile = Regressor_Settings

        # The parameters used for training, we want to keep on to these as class variables during training
        self.X = None
        self.y = None
        self.theta = None

        self.use_feature_scaling = use_feature_scaling  # Optimizes gradient descent
        self.use_normal_equation = use_normal_equation
        self.lambd = regularization_parameter


        # If feature scaling is used, we need to keep track of mu and sigma
        self.mu = None
        self.sigma = None

        self.debug = debug
        self.optimization_history = []

    def construct_X_matrix(self,x):

        if type(x) not in (list, np.ndarray):
            x = [x]  # We want to be able to iterate

        # Initialize dataset with features. Ignore product terms as those will be added later on
        # We make sure each featurefunc returns a list, regardless of whether it only returns one value or not.

        X = [sum([[1, xi] + sum([featurefunc(xi) for featurefunc in self.profile.features if\
                                 featurefunc != product_terms], [])], []) for xi in x]

        # Add product terms
        if product_terms in self.profile.features:
            X = np.array([row + product_terms(row[1:]) for row in X], dtype=np.float64)
            X.reshape((len(x), -1))
        else:
            X = np.array(X, dtype=np.float64)

        # If normal equation is used, feature scaling is not necessary
        if self.use_feature_scaling:
            X = self.normalize(X)

        return X


    def predict(self, x):

        """
        :param x: a single value, a list or a 1D array containing the time values for which a prediction is desired
        :return: the predicted value(s) at the supplied time value(s)
        """

        X = self.construct_X_matrix(x)
        predictions = X @ self.theta

        return predictions

    def normalize(self, X):

        tonorm = X[:, 1:].copy()

        if self.mu is None:
            self.mu = np.mean(tonorm, 0)
            self.sigma = np.std(tonorm, axis=0, dtype=np.float64)

        tonorm = (tonorm - self.mu) / self.sigma

        toreturn = np.ones(X.shape)
        toreturn[:, 1:] = tonorm
        return toreturn


    def track_optimization(self,curiter, curtheta):
        self.optimization_history.append((curiter,self.profile.J(self.X,curtheta,self.y,self.lambd)))


    def train(self, x, y, override_iterations=0):

        """
        :param x: the time values of the data set on which this regressor is to be trained
        :param y: the measured results at the provided time values
        :param override_iterations: an optional override for the amount of cycles the gradient descent optimization has
                                    to run for
        :return: a trained regressor object
        """

        self.X = self.construct_X_matrix(x)
        m, n = self.X.shape

        self.y = np.asarray(y, dtype=np.float64).reshape(m,1)

        if self.use_normal_equation:  # Directly calculate weights using Normal Equation
            eyematrix = np.eye(n, n, dtype=np.float64)
            eyematrix[0, 0] = 0
            self.theta = np.linalg.pinv(self.X.T @ self.X + self.lambd * eyematrix) @ self.X.T @ self.y
            return self

        # If normal equation is not used, we can start training the regressor

        # Initialize weights
        self.theta = np.zeros((n, 1), dtype=np.float64)
        self.optimization_history = []

        if override_iterations != 0:
            self.theta = self.profile.optimize(self,override_iterations)
        else:
            self.theta = self.profile.optimize(self)


        if self.debug:
            print("Debug mode active, plotting learning curve...")
            print("Contains history of {} iterations.".format(len(self.optimization_history)))
            plt.plot([r[0] for r in self.optimization_history], [r[1] for r in self.optimization_history])
            plt.title("Learning Curve: Cost vs Iteration")
            plt.xlabel("Iteration")
            plt.ylabel("J")
            plt.grid()
            plt.show()
        return self

# Class to build a compound function existing of multiple configured regressors. Not yet trained regressors
class Compound_Regressor:
    def __init__(self,trained_regressors=None,boundary_points=None):

        """
        :param trained_regressors: Only fill this in if you have pre-trained regressors!
        :param boundary_points: Only fill this in if you have pre-trained regressors!
        """

        if trained_regressors and boundary_points:
            tosort = zip(trained_regressors,boundary_points)
            sort = list(sorted(tosort,key=lambda k: k[1][0]))
            trained_regressors,boundary_points = zip(*sort)

            boundary_points = [list(bc) for bc in boundary_points]
            for i in range(len(boundary_points)-1):
                if boundary_points[i][1] != boundary_points[i+1][0]:
                    boundary_points[i][1] = boundary_points[i+1][0] = (boundary_points[i][1]+boundary_points[i+1][0])/2

        self.boundarypoints = boundary_points
        self.regressors = trained_regressors

    def train(self,regressors,trainingsets,iteration_overrides = None): # training sets of format [(xset1,yset1),...]

        """
        :param regressors: a list of pre-built regressors that have not yet been trained, but have been configured
        :param trainingsets: a list of data sets, one (x,y) data set for each regressor
        :param iteration_overrides: optional iteration overwrites that determine how many cycles the gradient descent
                                    optimization runs for
        :return: a trained compound regressor
        """

        if not iteration_overrides:
            iteration_overrides = [0]*len(regressors)

        boundary_points = [[ts[0][0],ts[0][-1]] for ts in trainingsets]
        for i in range(len(boundary_points)-1):
            if boundary_points[i][1] != boundary_points[i+1][0]:
                boundary_points[i][1] = boundary_points[i+1][0] = (boundary_points[i][1]+boundary_points[i+1][0])/2

        tosort = zip(regressors, boundary_points,iteration_overrides)

        sort = list(sorted(tosort, key=lambda k: k[1][0]))

        regressors, boundary_points, iteration_overrides = zip(*sort)

        self.regressors = regressors
        self.boundarypoints = boundary_points

        for index,regressor in enumerate(self.regressors):
            regressor.train(*trainingsets[index],override_iterations=iteration_overrides[index])

        return self


    def predict(self, x):

        """
        :param x: a single value, a list or a 1D numpy array containing the time value(s) for which a prediction is
                  desired
        :return: the predicted value(s) from the regressor(s) that is/are trained on the domain where the value(s)
                 is/are located
        """

        if not type(x) in (list,np.ndarray): x = [x]

        filters = []

        check_in_range = lambda x,bp: (bp[0] < x <= bp[1])

        filters.append(np.array([1 if xval<=self.boundarypoints[0][0] else 0 for xval in x]).reshape(len(x),1))

        for i in range(len(self.boundarypoints)):
            newfilter = [1 if check_in_range(xval,self.boundarypoints[i]) else 0 for xval in x]
            filters.append(np.array(newfilter).reshape(len(x),1))

        filters.append(np.array([1 if xval > self.boundarypoints[-1][1] else 0 for xval in x]).reshape(len(x), 1))

        results = [self.regressors[regid].predict(x) for regid in [0,*list(range(len(self.regressors))),-1]]
        toreturn = sum([results[i]*filters[i] for i in range(len(filters))])


        return np.array(toreturn)
