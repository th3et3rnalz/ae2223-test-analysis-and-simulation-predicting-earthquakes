import _03_data_analysis.Linear_Regression as LR
import _03_data_analysis.time_series_conversion as TSC
import _03_data_analysis.data_function_fitting as DFF
import _03_data_analysis.fourier_transform as FT
import _03_data_analysis.earthquake_prediction as EP
from _03_data_analysis.RNN import TrainAndPredictRNN

TimeseriesConverter = TSC.Timeseries_Converter
DFF = DFF.Function_Fitting()
EP = EP.EarthquakePrediction


def clean_fourier(data):
    FourierTransform = FT.Fourier(data)
    without_seasonal = FourierTransform.periodic_variations()
    return without_seasonal

