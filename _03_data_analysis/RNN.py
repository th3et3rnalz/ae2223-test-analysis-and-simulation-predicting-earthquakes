import os
os.environ['CUDA_VISIBLE_DEVICES'] = ""
# The above two lines are to prevent tensorflow from using CUDA.
# Normally you don't want to do this, but for some reason it fails when
# trying to use CUDA and I just don't have the time to fix it.
import tensorflow as tf
import numpy as np
import pandas as pd
from datetime import timedelta, datetime
import pickle


class TrainAndPredictRNN:
    def __init__(self, dataset, earthquakes, manual_earthquakes=True):
        self.data = dataset
        self.earthquakes = earthquakes.to_pydatetime()

        if manual_earthquakes:
            self.earthquakes = np.array([datetime(2004, 11, 26, 0, 0)], dtype=object)  # I'm sure there are more, but...

        self.standardize_features = False
        self.BATCH_SIZE = 20
        self.BUFFER_SIZE = 10000
        self.PREDICT_SIZE = 400
        self.EPOCHS = 10
        self.STEP_PER_EPOCH = 10
        self.min_dataset_size = self.EPOCHS * self.STEP_PER_EPOCH
        self.TRAIN_RATIO = 0.8
        self.STEP_SIZE = 6
        self.DATA_SCALING = 10**3
        self.model = False
        self.last_points_dataset = {}
        self.predictions = {}
        self.train_history = []

    def make_predictions(self, print_them=True):
        self.model = tf.keras.models.load_model('_03_data_analysis/cache/tf_model')
        f = open('_03_data_analysis/cache/last_points_dataset.pickle', 'rb')
        last_points_dataset = pickle.load(f)
        f.close()

        for name, value in last_points_dataset.items():
            X, last_date = value
            X = X.values * self.DATA_SCALING  # The model was trained with these scaled inputs to reduce round off error
            if self.standardize_features:
                X = X - X.mean(axis=0)
            X.shape = (1, X.shape[0], X.shape[1])
            days_after_last_measurement = self.model.predict(X)[0][0] * 365.242
            self.predictions[name] = [last_date + timedelta(days=days_after_last_measurement),
                                      days_after_last_measurement]

        if print_them:
            print("\n\n\n")
            [print(f"{name}: {str(val[0])}, {val[1]:.2f}  days") for name, val in self.predictions.items()]

        return self.predictions

    def create_and_train_model(self):
        # ------------------------------------------------------------------------------------------
        #         TRAINING THE MODEL     TRAINING THE MODEL      TRAINING THE MODEL
        # ------------------------------------------------------------------------------------------

        for name, df in self.data.groupby('name'):

            val = self.prepare(df)
            if val is not None:
                val_data, train_data, batch_shape, x_to_predict, last_date = val
                self.last_points_dataset[name] = [x_to_predict, last_date]

                if not self.model:
                    self.model = self.create_model(batch_shape)

                single_step_history = self.model.fit(train_data, epochs=self.EPOCHS,
                                                     steps_per_epoch=self.STEP_PER_EPOCH,
                                                     validation_data=val_data,
                                                     validation_steps=50)

                self.train_history.append(single_step_history.history['val_loss'])

        # Let's save the model:
        self.model.save('_03_data_analysis/cache/tf_model')

        # Also save the data necessary to make predictions
        with open('_03_data_analysis/cache/last_points_dataset.pickle', 'wb') as f:
            pickle.dump(self.last_points_dataset, f)

    def transform(self, df):
        if df.shape[0] < self.min_dataset_size:
            return None

        timedelta = pd.to_timedelta(df.index.to_series().diff())
        timedelta_integer_days = np.array([val.days for val in timedelta])
        df['dt'] = timedelta_integer_days
        df = df[df['dt'] != 0]
        df['vlat'] = df['lat (deg N) (m)'].diff().fillna(0.) / df['dt']
        df['vlon'] = df['lon (deg E) (m)'].diff().fillna(0.) / df['dt']
        df['vrad'] = df['rad (m)'].diff().fillna(0.)

        X = df.loc[df.index[1]:, 'vlat':'vrad']
        # The above variable contains the features for training

        first_date = df.index[0]
        last_date = df.index[-1]

        earthquakes_in_dataset = []
        for date in self.earthquakes:
            if first_date < date < last_date:
                earthquakes_in_dataset.append(date)

        if len(earthquakes_in_dataset) == 0:
            return None

        # Now we need to create a list that is as long as the dataset, but containing the relevant label.
        y = []
        label_day = earthquakes_in_dataset[0]
        label_index = 0
        stop = False
        for value_day in df.index.to_pydatetime():
            if value_day >= label_day:
                label_index += 1
                if label_index >= len(earthquakes_in_dataset):
                    # We don't care about datapoints for which we have no label
                    stop = True
                else:
                    label_day = earthquakes_in_dataset[label_index]
            if stop:
                break
            y.append((label_day - value_day).days / 365.242)

        # Now we need to get the latest values from the model so we can make some prediction:
        x_to_predict = X[-self.PREDICT_SIZE:]

        # Given that y is shorter than X, we need to shorten X. We only care about the features until the last
        # earthquake. All the data after that is data that will be fed into the RNN when finding a prediction.
        X = X.values[:len(y)] * self.DATA_SCALING
        y = np.array(y)

        # Now we need to standardize the features
        if self.standardize_features:
            X = X - X.mean(axis=0)

        total_amount_of_examples = len(y)
        TRAIN_SPLIT = int(self.TRAIN_RATIO * total_amount_of_examples)

        past_history = self.PREDICT_SIZE
        future_target = 1

        x_train_single, y_train_single = multivariate_data(X, y, 0,
                                                           TRAIN_SPLIT, past_history,
                                                           future_target, self.STEP_SIZE,
                                                           single_step=True)
        x_val_single, y_val_single = multivariate_data(X, y,
                                                       TRAIN_SPLIT, None, past_history,
                                                       future_target, self.STEP_SIZE,
                                                       single_step=True)

        return x_train_single, x_val_single, y_train_single, y_val_single, x_to_predict, last_date.to_pydatetime()

    def create_model(self, shape):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(10, input_shape=shape))
        # model.add(tf.keras.layers.LSTM(10))
        model.add(tf.keras.layers.Dense(1))
        model.compile(optimizer=tf.keras.optimizers.RMSprop(), loss='mse')
        print(model.summary())
        return model

    def prepare(self, df):
        val = self.transform(df)
        if val is not None:
            x_train, x_validate, y_train, y_validate, x_to_predict, last_date = val
            train_data_single = tf.data.Dataset.from_tensor_slices((x_train, y_train))
            train_data_single = train_data_single.cache().shuffle(self.BUFFER_SIZE).batch(self.BATCH_SIZE).repeat()

            val_data_single = tf.data.Dataset.from_tensor_slices((x_validate, y_validate))
            val_data_single = val_data_single.batch(self.BATCH_SIZE).repeat()

            # TODO: FIX THIS SHIT. the problem, is that sometimes there is no training and or validation data
            if np.sum(x_train) == 0 or np.sum(x_validate) == 0:
                return None

            return train_data_single, val_data_single, x_train.shape[-2:], x_to_predict, last_date
        return None


def multivariate_data(dataset, target, start_index, end_index, history_size,
                      target_size, step, single_step=False):
    data = []
    labels = []

    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size

    for i in range(start_index, end_index):
        indices = range(i - history_size, i, step)
        data.append(dataset[indices])

        if single_step:
            labels.append(target[i + target_size])
        else:
            labels.append(target[i:i + target_size])

    return np.array(data), np.array(labels)
