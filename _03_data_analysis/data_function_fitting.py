import _03_data_analysis.Linear_Regression as LR
import pickle
import numpy as np
import datetime
import matplotlib.pyplot as plt
import matplotlib
import os



class Function_Fitting:
    def __init__(self):
        self.time_series = None

        self.regressors = {}
        self.iter_override = 10000
        self.allow_normal = True


    def plot_functions(self, splitindices, station_name = None):

        # Disable plotting windows
        matplotlib.use('Agg')

        to_iterate = self.time_series if not station_name else [station_name]

        for station, splitindex in zip(to_iterate, splitindices):
            print(f"Plotting regression plot for {station}")
            time = np.array(self.time_series[station]["Time"])
            predict_time = np.arange(time[0],time[-1],0.5)

            plt.figure()

            lon = self.time_series[station]["lon"]
            lat = self.time_series[station]["lat"]
            rad = self.time_series[station]["rad"]

            lon = (lon - lon[splitindex]) * 10 ** 7
            lat = (lat - lat[splitindex]) * 10 ** 7
            rad = (rad - rad[splitindex]) * 10 ** 2


            plt.subplot(3,1,1)
            plt.title(station + " ## " + str(self.time_series[station]["Reference"]))
            plt.ylabel("lon")
            plt.scatter(time,lon, color="r", s=1, marker="+")

            plt.tick_params(
                axis="x",
                which="both",
                bottom=False,
                top=False,
                labelbottom=False
            )

            plt.plot(predict_time, self.regressors[station]["lon"].predict(predict_time))

            plt.subplot(3, 1, 2)
            plt.ylabel("lat")
            plt.scatter(time, lat, color="r", s=1, marker="+")

            plt.tick_params(
                axis="x",
                which="both",
                bottom=False,
                top=False,
                labelbottom=False
            )

            plt.plot(predict_time,
                     self.regressors[station]["lat"].predict(predict_time))

            plt.subplot(3, 1, 3)
            plt.ylabel("rad")
            plt.scatter(time, rad, color="r", s=1, marker="+")
            plt.plot(predict_time,
                     self.regressors[station]["rad"].predict(predict_time))
            plt.xlabel("days since beginning of measurements")
            if 'RegPlots' not in os.listdir('./_03_data_analysis'):
                os.mkdir('./_03_data_analysis/RegPlots')
            plt.savefig(f"./_03_data_analysis/RegPlots/regressed_{station}.png", bbox_inches="tight")
            plt.close()

        # Re-enable plotting windows
        matplotlib.use('TkAgg')


    def attempt_regression(self,time, displacement, profile, regularization=0):

        # Try to use the normal equation IF it is enabled. If it is not, it will do the same thing as the except
        try:
            regressor = LR.Regressor(profile,
                                     use_normal_equation=self.allow_normal, regularization_parameter=regularization
                                     ).train(time,displacement, override_iterations= self.iter_override)

        # If it fails, use gradient descent
        except:
            print("    Using GD")
            regressor = LR.Regressor(profile, regularization_parameter=regularization).train(
                time,displacement, override_iterations=self.iter_override
            )
        return regressor

    def fit_functions(self, time_series, use_normal = True, normal_exceptions = None, use_all_earthquakes=tuple(),
                      only_plot=None, plotting=False):

        self.allow_normal = use_normal
        self.time_series = time_series

        normal_exceptions = {normal_exceptions, use_all_earthquakes} - {()}

        all_earthquakes = [datetime.datetime(2004,12,26),
                           datetime.datetime(2005,3,28),
                           datetime.datetime(2012,4,11)]


        regressor_profile_lin = LR.Regression_Profile(LR.least_squares, LR.derivative_least_squares, [],
                                                      LR.batch_gradient_descent(0.01))

        regressor_profile_exp = LR.Regression_Profile(LR.least_squares, LR.derivative_least_squares,
                                            [LR.exp_alt(-1/365,0), LR.log_alt(1/365, 1)], LR.batch_gradient_descent(0.01))

        regressor_profile_lin_sinusoidal = LR.Regression_Profile(LR.least_squares, LR.derivative_least_squares,
                                                                 [*[LR.sin_alt(n/365,0) for n in range(1,15)]],
                                                                 LR.batch_gradient_descent(0.01))

        regressor_profile_exp_sinusoidal = LR.Regression_Profile(LR.least_squares, LR.derivative_least_squares,
                                                                 [LR.exp_alt(-1/365,0), LR.log_alt(1/365,1),
                                                                  *[LR.sin_alt(n/365,0) for n in range(1,15)]],
                                                                 LR.batch_gradient_descent(0.01))

        for curindex, (station, data) in enumerate(self.time_series.items()):
            if only_plot:
                if not station in only_plot:
                    continue

            if len(normal_exceptions) > 0:
                if station in normal_exceptions:
                    self.allow_normal = False
                else:
                    self.allow_normal = use_normal

            print(f"Regressing station nr {curindex + 1} out of {len(self.time_series)}: {station}...")

            time = np.array(data["Time"])
            lon = data["lon"]
            lat = data["lat"]
            rad = data["rad"]

            # If we want to incorporate all major earthquakes, more date offsets are required
            if station in use_all_earthquakes:
                offsets = [(quake_date - data["Reference"]).days for quake_date in all_earthquakes]
            else:
                offsets = [(all_earthquakes[0] - data["Reference"]).days]

            splitting_indices = []
            split_list = lambda i, tosplit: [tosplit[:i],tosplit[i:]]

            for offset in offsets:
                date_filter = [date > offset for date in time]
                splitting_indices.append(date_filter.index(True))

            # We normalize the data wrt the Sumatra earthquake
            normalization_index = max(0, splitting_indices[0] - 1)
            lon = (lon - lon[normalization_index]) * 10 ** 7
            lat = (lat - lat[normalization_index]) * 10 ** 7
            rad = (rad - rad[normalization_index]) * 10 ** 2


            # We try the regression regardless of whether we have enough datapoints for it
            split_time = [time]
            split_lon = [lon]
            split_lat = [lat]
            split_rad = [rad]

            last_split = 0
            for split_index in splitting_indices:
                if split_index < 0:
                    split_index = 0
                split_time = split_time[:-1] + split_list(split_index - last_split, split_time[-1])
                split_lon = split_lon[:-1]   + split_list(split_index - last_split, split_lon[-1])
                split_lat = split_lat[:-1]   + split_list(split_index - last_split, split_lat[-1])
                split_rad = split_rad[:-1]   + split_list(split_index - last_split, split_rad[-1])
                last_split = split_index

            lon_regressors = []
            lat_regressors = []
            rad_regressors = []
            bc = []

            profiles = [regressor_profile_lin] + [regressor_profile_exp]*len(splitting_indices)
            rprofiles = [regressor_profile_lin_sinusoidal] + [regressor_profile_exp_sinusoidal]*len(splitting_indices)

            for i in range(len(split_time)):
                if len(split_time[i]) == 0:
                    continue
                lon_regressors.append(self.attempt_regression(split_time[i], split_lon[i], profiles[i]))
                lat_regressors.append(self.attempt_regression(split_time[i], split_lat[i], profiles[i]))
                rad_regressors.append(self.attempt_regression(split_time[i], split_rad[i], rprofiles[i]))
                bc.append((split_time[i][0], split_time[i][-1]))

            self.regressors[station] = {
                "lon": LR.Compound_Regressor(lon_regressors, bc),
                "lat": LR.Compound_Regressor(lat_regressors, bc),
                "rad": LR.Compound_Regressor(rad_regressors, bc),
                "zero_lon": data["lon"][normalization_index],
                "zero_lat": data["lat"][normalization_index]
            }

            if plotting:
                self.plot_functions([normalization_index], station_name=station)

        return self
