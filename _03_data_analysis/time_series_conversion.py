import pandas as pd
import numpy as np
import pickle


class Timeseries_Converter:
    @staticmethod
    def convert_to_time_series(d14_data, use_cached=False, datetime_index=False):

        """
        :param d14_data: the pandas object of all the converted and filtered d14 data
        :param use_cached: make a pickle file of this time series dict?
        :return: complete time series dict: the time series of every station in one large dict object (not pandas)
        """

        if use_cached:
            try:
                print("Attempting to load time series pickle...")
                with open("_03_data_analysis/cache/time_series.pickle", "rb") as file:
                    toreturn = pickle.load(file)
                return toreturn
            except:
                print("Time series pickle not present, re-building time series...")
        else:
            print("Building time series...")
        stations = d14_data["name"].unique()
        time_series = {}
        for station in stations:
            time_series[station] = {}

            station_data = d14_data[d14_data["name"] == station]
            datetime_data = station_data.index if datetime_index else [station_index[0] for station_index in station_data.index]
            reference_date = datetime_data[0]

            time_series[station]["Reference"] = pd.to_datetime(reference_date, format="%Y-%m-%d")

            time_series[station]["Time"] = [(pd.to_datetime(date) - pd.to_datetime(reference_date)).days for date in datetime_data]
            time_series[station]["lon"] = station_data["lon (deg E) (m)"].values
            time_series[station]["lat"] = station_data["lat (deg N) (m)"].values
            time_series[station]["rad"] = station_data["rad (m)"].values

        with open("_03_data_analysis/cache/time_series.pickle", "wb") as file:
            pickle.dump(time_series, file)

        return time_series

    # Function to linearly interpolate for any missing days in the data. Could be useful for the RNN
    @staticmethod
    def fill_station_data_gaps(station_time_series):

        """
        :param station_time_series: the time series dict of a single (!!) station
        :return: a time series dict of the supplied station with all gaps filled using linear interpolation
        """

        newtime = []
        newlon = []
        newlat = []
        newrad = []

        interpolated_values = []

        for i in range(len(station_time_series["Time"]) - 1):
            thisday = station_time_series["Time"][i]
            nextday = station_time_series["Time"][i + 1]

            lon1, lon2 = station_time_series["lon"][i], station_time_series["lon"][i + 1]
            lat1, lat2 = station_time_series["lat"][i], station_time_series["lat"][i + 1]
            rad1, rad2 = station_time_series["rad"][i], station_time_series["rad"][i + 1]

            newtime.append(thisday)
            newlon.append(lon1)
            newlat.append(lat1)
            newrad.append(rad1)

            # The actual dates are not interpolated, so we don't mark them as such
            interpolated_values.append(0)

            # We don't run this loop if the difference is one day
            for j in range(1, nextday - thisday, 1):
                interp_lon = (lon2 - lon1) / (nextday - thisday) * j + lon1
                interp_lat = (lat2 - lat1) / (nextday - thisday) * j + lat1
                interp_rad = (rad2 - rad1) / (nextday - thisday) * j + rad1

                newtime.append(thisday + j)
                newlon.append(interp_lon)
                newlat.append(interp_lat)
                newrad.append(interp_rad)

                # Now there is an interpolated value, so it must be marked as such
                interpolated_values.append(1)

        # Add the final datapoints to the arrays and mark it as not interpolated
        newtime.append(station_time_series["Time"][-1])
        newlon.append(station_time_series["lon"][-1])
        newlat.append(station_time_series["lat"][-1])
        newrad.append(station_time_series["rad"][-1])
        interpolated_values.append(0)


        new_time_series = {}
        new_time_series["Reference"] = station_time_series["Reference"]
        new_time_series["Time"] = newtime
        new_time_series["lon"] = np.asarray(newlon)
        new_time_series["lat"] = np.asarray(newlat)
        new_time_series["rad"] = np.asarray(newrad)

        # Convert it to an array for ease of use
        interpolated_values = np.asarray(interpolated_values, dtype=np.int)

        return new_time_series, interpolated_values
