import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import fft, interpolate, optimize
import _03_data_analysis.time_series_conversion as time_series_conversion

mpl.use('QT5Agg')
mpl.rc('figure', max_open_warning=0)


class Fourier:
    def __init__(self, dataset, debug=True):
        # Making a data copy of the dataset
        self.debug = debug
        self.dataset = dataset.copy()
        self.clean_dict={}
        self.variations_dict = {}
        
        

        # assert list(self.dataset.columns.values) == ['name', 'lat (deg N) (m)', 'lat_dev (cm)',
        #                                              'lon (deg E) (m)', 'lon_dev (cm)', 'rad (m)', 'rad_dev (cm)',
        #                                              'country']

    @staticmethod
    def fit_curve(x, a, b, c, d, e):
        """
        Model fit curve for the frequency analysis. Optimised in periodic_variations code to fit.
        """
        return a * np.exp(-b * x ** 2) + c * np.exp(-d * x ** 2) + e

    def periodic_variations(self, plot=False):
        """
        Finds and removes periodic components from the input radial displacement data

        Returns
        -------
        clean_dict : Dictionary of stations and RADIAL COMPONENT OF DISPLACEMENT

        """
        a = time_series_conversion.Timeseries_Converter.convert_to_time_series(self.dataset, use_cached=True)
        clean_dict = {}
        variations_dict = {}
        for key in a.keys():
            #WUHN, BAKO, MAC1 excluded for exceedingly large gaps in data, KINA for poor returned results
            if len(a[key]['Time']) >= 500 and key not in ['WUHN', 'BAKO', 'MAC1', 'KINA']:
                print('finding variations in', key)
                #Fill gaps in days
                interp, interp_filter = time_series_conversion.Timeseries_Converter.fill_station_data_gaps(a[key])
                time = interp['Time'].copy()
                originalvalues = interp['rad'].copy()
                values = interp['rad'].copy()

                #fit a line, 'bash', to the function data. Subtract this line from the data
                #to try to remove the non-periodic component of the signal as much as possible
                s = interpolate.UnivariateSpline(time, values, s=len(time) * 100)
                bash = s(time)
                values = values - bash

                #Take the fourier transform of the signal. fft.fft returns the frequencies in a stupid order,
                #so we sort the frequencies and their magnitudes. variable 'order' keeps track of the original
                #order of the signal, so we can unsort the list to the correct format for fft.ifft to revert the 
                #values back to the time domain
                y = fft.fft(values)
                freqs = fft.fftfreq(len(values))
                order = [i for i in range(len(freqs))]
                freqs, y, order = (np.asarray(t) for t in zip(*sorted(zip(freqs, y, order))))

                #fft returns complex values, where abs() gives the magnitude of the frequency present in the signal
                #and arg() gives the phase shift. Scale the frequency magnitude so that the maximum magnitude is 1.
                #This enables us to fit similar curves to all the values
                frequency_magnitudes = abs(y)
                scale_factor = 1 / max(frequency_magnitudes)
                scaled_frequency_magnitudes = frequency_magnitudes * scale_factor
                try:
                    #Optimise fit curve to match frequency magnitude graph
                    popt, pcov = optimize.curve_fit(Fourier.fit_curve, freqs, scaled_frequency_magnitudes,
                                                    p0=[1, 400, 0.3, 50, 0])
                except:
                    print(f"Curve fit failed for {key}")
                    clean_dict[key] = a[key].copy()
                    continue

                fitline = self.fit_curve(freqs, *popt)

                #residual values are the frequency magnitudes minus the fit line.
                #Take the standard deviation of the whole frequency magnitude spectrum
                residuals = scaled_frequency_magnitudes - fitline
                fallback_sd = np.std(residuals)

                #We only want the peaks of the frequency magnitudes - these are the most significant components
                #of frequency in the signal. 
                #Since the variance of the frequency magnitude changes greatly through the signal, we need to use
                #the local standard deviation (values+-50 from the current value) where possible.
                #We only want signals where the residual is greater than 3sd above the fit line
                #In addition, we can exclude signals with a period shorter than about 14 days or about 365 years
                #with a small amount of wiggle-room allowed
                onlypeaks = list(y)
                for i in range(len(onlypeaks)):
                    if i in range(50,len(onlypeaks)-50):
                        sd = np.std(residuals[i-50:i+50])
                    else:
                        sd = fallback_sd
                    if residuals[i] < 3 * sd:
                        onlypeaks[i] = 0
                    if abs(freqs[i])>0.08 or abs(freqs[i])<0.002:
                        onlypeaks[i] = 0

                for i in range(len(onlypeaks)//2):
                    if onlypeaks[i] != 0:
                        onlypeaks[-(i+1)]=y[-(i+1)]
                        
                #Keep a copy of the onlypeaks list for plotting graphs.
                #Unsort the list to the format required by fft.ifft using the order list from earlier
                orderedonlypeaks = np.asarray(onlypeaks)
                order, onlypeaks = (np.asarray(t) for t in zip(*sorted(zip(order, onlypeaks))))

                # inverse fast fourier to flip back to the time domain
                filtered_signal = np.real(fft.ifft(onlypeaks))

                #cleaned signal is the original signal minus the values
                cleaned_signal = originalvalues - filtered_signal

                if plot:
                    #TOP LEFT: Frequency magnitudes, with peaks chosen by algorithm shown
                    #TOP RIGHT: Scaled frequency magnitudes, with the fit line superimposed
                    #BOTTOM LEFT: Signal without non-periodic data (guess), with true periodic variation data superimposed
                    #BOTToM RIGHT: Original signal, with guess at fit line and final cleaned signal superimposed.
                    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
                    fig.suptitle(key)

                    ax1.plot(freqs, np.abs(y), label='Spectrum magnitude of periodic variations')
                    ax1.plot(freqs, np.abs(orderedonlypeaks), label='Only peak values')
                    ax1.set_xlabel('Frequency in 1/days')
                    ax1.set_ylabel('Frequency Domain (Spectrum) Magnitude')
                    ax1.legend()

                    ax2.plot(freqs, scaled_frequency_magnitudes, label='Scaled frequency magnitudes')
                    ax2.plot(freqs, fitline, label='Frequency fit line')
                    ax2.set_xlabel('Frequency in 1/days')
                    ax2.set_ylabel('Frequency Domain (Spectrum) Magnitude')
                    ax2.legend()

                    ax3.plot(time, values, label='Periodic variation data')
                    ax3.plot(time, filtered_signal, label='Fourier series fit to periodic variation')
                    ax3.set_xlabel('Days')
                    ax3.set_ylabel('periodic Displacement')
                    ax3.legend()

                    ax4.plot(time, originalvalues, label='Original input data')
                    ax4.plot(time, cleaned_signal, '--', label='Data with periodic variation removed')
                    ax4.plot(time, bash, label='Guess at a fit line')

                    ax4.set_xlabel('Days')
                    ax4.set_ylabel('Total Displacement')
                    ax4.legend()

                    plt.show()
                
                #store in dictionaries
                clean_dict[key] = a[key].copy()
                clean_dict[key]["rad"] = cleaned_signal[interp_filter == 0].copy() # We only want the original values
                variations_dict[key] = filtered_signal[interp_filter == 0].copy()
            else:
                clean_dict[key] = a[key].copy()
        
        #keep copies in the instance
        self.clean_dict = clean_dict.copy()
        self.variations_dict = variations_dict.copy()
                
        return clean_dict
