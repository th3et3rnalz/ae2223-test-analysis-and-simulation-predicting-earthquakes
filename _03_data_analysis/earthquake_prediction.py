import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import pickle
from pathlib import Path


class EarthquakePrediction:
    def __init__(self, time_series, regressors):
        self.time_series = time_series
        self.regressors = regressors

    def extrapolate_data(self,end_date=dt.datetime(2620,12,31), step_size=1, stations = None, use_cached=False):
        extrapolated = {}
        to_extrapolate = stations or list(self.regressors.keys())

        picklepath = Path("./_03_data_analysis/cache/extrapolated_data.pickle")

        if use_cached and picklepath.exists():
            with open("./_03_data_analysis/cache/extrapolated_data.pickle","rb") as file:
                extrapolated_cached = pickle.load(file)
            extrapolated = {station: data for station, data in extrapolated_cached.items() if station in to_extrapolate}
            return extrapolated
        for station in to_extrapolate:
            # reference = self.time_series[station]["Reference"]
            end_day = 350*365
            prediction_range = np.arange(0,end_day + step_size,step_size)

            lon_predicted = self.regressors[station]["lon"].predict(prediction_range)
            lat_predicted = self.regressors[station]["lat"].predict(prediction_range)
            rad_predicted = self.regressors[station]["rad"].predict(prediction_range)

            extrapolated[station] = {}

            extrapolated[station]["Time"] = prediction_range
            extrapolated[station]["lon"] = lon_predicted
            extrapolated[station]["lat"] = lat_predicted
            extrapolated[station]["rad"] = rad_predicted

        with open(picklepath, "wb") as file:
            pickle.dump(extrapolated, file)

        return extrapolated

    def plot_extrapolation(self, extrapolation_dict, stations = None):
        to_plot = stations if stations else extrapolation_dict.keys()

        for station in to_plot:
            plt.plot(extrapolation_dict[station]["Time"], extrapolation_dict[station]["lon"])

    def detect_quakes(self, extrapolated_data, position_error_bound=2, slope_error_bound=1e-12):

        """This function has been deprecated but is kept around for reference"""

        quake_date = dt.datetime(2004,12,26)

        all_predicted_dates = []

        for station, data in extrapolated_data.items():
            reference_date = self.time_series[station]["Reference"]
            quake_offset = (quake_date-reference_date).days

            # Use the predicted value as the error is not too great here
            o_lon, o_lat = [self.regressors[station][reg].predict(quake_offset) for reg in ("lon", "lat")]

            lon_match = lambda x: abs(x - o_lon) * 10 ** 7 <= position_error_bound #* o_lon
            lat_match = lambda x: abs(x - o_lat) * 10 ** 7 <= position_error_bound #* o_lat

            predicted_dates = set()
            reference_year = reference_date.year

            # print("----------------------")
            for index in range(len(data["lon"])):
                # print(data["lon"][index] - o_lon)
                # print(data["lat"][index] - o_lat)
                # print("---")
                # print(abs(data["lon"][index] - o_lon) * 10**7)
                if (lon_match(data["lon"][index]) or lat_match(data["lat"][index])) \
                        and (data["Time"][index] > quake_offset + 10*365):
                    predicted_dates.add(reference_year + data["Time"][index]//365)
                    # print(predicted_date)

            all_predicted_dates.append(predicted_dates)

        # print(all_predicted_dates[0].intersection(*all_predicted_dates[1:]))
        print(all_predicted_dates)

    def analyze_offsets(self, to_analyze):
        """
        This function attempts to get more accurate earthquake predictions for stations beginning measurements after
        2004. It does this by looking at inter-station relations and guessing a station position right before the 2004
        earthquake.
        Only a few stations are worth analyzing in this way, namely if they have enough reference datapoints to get an
        accurate offset analysis.
        """

        analyzed_dict = {}

        # Create a reduced copy of the time series dict
        to_analyze = {key: value for key, value in self.time_series.items() if key in to_analyze}
        for station, data in to_analyze.items():
            reference = data["Reference"]

            # Look for close-by stations
            minimum_distance_station = ""
            current_min_lon = 9999999
            current_min_lat = 9999999

            for otherstation, otherdata in self.time_series.items():
                # Don't need to offset station by itself
                if otherstation == station:
                    continue

                # If measurements started after station under consideration or ended before, we don't need it
                if otherdata["Reference"] > reference \
                        or otherdata["Reference"] + dt.timedelta(days=otherdata["Time"][-1]) < reference \
                        or otherdata["Reference"] > dt.datetime(2004, 12, 26):
                    continue
                # For comparing distances, let's take a date which is in both time series and look at the closest points
                lower_bound = reference
                upper_bound = min(reference + dt.timedelta(days=data["Time"][-1]),
                                  otherdata["Reference"] + dt.timedelta(days=otherdata["Time"][-1]))
                comparison_date = lower_bound + dt.timedelta(days= (upper_bound - lower_bound).days//2)

                station_offset = (comparison_date - reference).days
                otherstation_offset = (comparison_date - otherdata["Reference"]).days

                # This is where I regret not making "Time" a numpy array, but let's do it like this for now
                station_closest_index = [val >= station_offset for val in data["Time"]].index(True)
                otherstation_closest_index = [val >= otherstation_offset for val in otherdata["Time"]].index(True)

                lon_offset = abs(otherdata["lon"][otherstation_closest_index] - data["lon"][station_closest_index])
                lat_offset = abs(otherdata["lat"][otherstation_closest_index] - data["lat"][station_closest_index])

                if lon_offset < current_min_lon and lat_offset < current_min_lat:
                    current_min_lon = lon_offset
                    current_min_lat = lat_offset
                    minimum_distance_station = otherstation

            # We can now look at offsets
            minimumreference = self.time_series[minimum_distance_station]["Reference"]
            closest_station_time = self.time_series[minimum_distance_station]["Time"]
            closest_station_lon = self.time_series[minimum_distance_station]["lon"]
            closest_station_lat = self.time_series[minimum_distance_station]["lat"]

            # We have to offset it to find corresponding entries
            station_time_offset = np.array(data["Time"]) + (reference - minimumreference).days

            # We require two separate filters because the station time lists have different lengths and start at
            # different times.
            filter_station = [True if val in closest_station_time else False for val in station_time_offset]
            filter_closest = [True if val in station_time_offset else False for val in closest_station_time]

            lon_offsets = (data["lon"][filter_station] - closest_station_lon[filter_closest])
            lat_offsets = (data["lat"][filter_station] - closest_station_lat[filter_closest])

            plt.subplot(211)
            plt.title(f"Relative position offsets {station} wrt {minimum_distance_station}")
            dirpath = Path("./_03_data_analysis/Offset_Analysis/")
            if not dirpath.exists():
                dirpath.mkdir()
            plt.plot(np.array(data["Time"])[filter_station], lon_offsets, label="Lon Offsets")
            plt.legend()
            plt.subplot(212)
            plt.plot(np.array(data["Time"])[filter_station], lat_offsets, label="Lat offsets")
            plt.legend()
            plt.savefig(dirpath.joinpath(f"Test-{station}.png"), bbox_inches="tight")
            plt.close()
            print(f"Closest station for {station} is {minimum_distance_station}")

            # We are going to make a pretty large assumption here: we are neglecting the inter-station elasticity
            # so we are going to take the mean of the offsets and treat it all as a constant offset. If you look at
            # The plots generated by this function, you will see this is not the case, but the offsets are very small.

            lon_offset = np.mean(lon_offsets)
            lat_offset = np.mean(lat_offsets)
            analyzed_dict[station] = {}
            analyzed_dict[station]["Reference_station"] = minimum_distance_station
            analyzed_dict[station]["lon"] = lon_offset
            analyzed_dict[station]["lat"] = lat_offset

        return analyzed_dict


    def detect_quakes_rootfinding(self, position_error_bound=0.01, offset_analysis = None):

        """This method does not need extrapolated data prepared as it uses recursive bisection to find the result"""

        quake_date = dt.datetime(2004,12,26)

        if offset_analysis:
            analyzed_offsets = self.analyze_offsets(offset_analysis)

        for station, regressors in self.regressors.items():
            print("-----------------------")
            print(f"Currently predicting quakes for {station}")

            # Get the date offset
            reference = self.time_series[station]["Reference"]
            reference_year = reference.year
            reference_offset = (quake_date - reference).days

            reg_offset_lon = 0
            reg_offset_lat = 0

            if offset_analysis:
                if station in offset_analysis:
                    analysis_data = analyzed_offsets[station]
                    ref_station = analysis_data["Reference_station"]

                    # Obtain zero measurements for this station
                    station_zero_lon = self.regressors[station]["zero_lon"]
                    station_zero_lat = self.regressors[station]["zero_lat"]

                    # Obtain zero measurements for the reference station
                    ref_zero_lon = self.regressors[ref_station]["zero_lon"]
                    ref_zero_lat = self.regressors[ref_station]["zero_lat"]

                    # Get the offsets from the analysis
                    lon_analysis_offset = analysis_data["lon"]
                    lat_analysis_offset = analysis_data["lat"]

                    # Apply analyzed offsets
                    reg_offset_lon = ((ref_zero_lon + lon_analysis_offset) - station_zero_lon) * 10 ** 7
                    reg_offset_lat = ((ref_zero_lat + lat_analysis_offset) - station_zero_lat) * 10 ** 7

            # If measurements started after the Earthquake, the resulting analysis is not entirely trustworthy.
            if reference_offset < 0:
                print(f"Measurements started on {reference}, "
                      f"after 26/12/2004. Therefore, results from this station are not entirely correct.")
                cont_bool = True
                if offset_analysis:
                    if station in offset_analysis:
                        print("Reference offset analysis has been performed on this station, continuing prediction.")
                        cont_bool = False
                        reference_offset = 0
                        print("Reference offsets (lon, lat): ", reg_offset_lon, reg_offset_lat)
                if cont_bool:
                    print("Skipping this station for earthquake prediction.")
                    continue

            # Get the necessary regressors for this station and offset them by the value predicted at the offset date.
            # This is done to improve analysis for stations whose measurements start during post-seismic deformation
            reglon = lambda x: regressors["lon"].predict(x) - reg_offset_lon
            reglat = lambda x: regressors["lat"].predict(x) - reg_offset_lat

            # Start the search a year after the Sumatra quake or beginning of measurements, to be safe
            a0 = reference_offset + 365

            # End the search in 700 years, as our predictions become useless after that time
            b0 = reference_offset + 700 * 365

            # Prediction for longitude:
            mult = reglon(a0) * reglon(b0)
            if mult > 0 or np.isnan(mult):
                print("Longitude never returns to original position within given range.")
            else:
                predicted = self.recursive_bisection(a0, b0, reglon, position_error_bound) // 365
                print(f"Predicted year according to lon: {reference_year + predicted}")

            # Prediction for latitude:
            mult = reglat(a0) * reglat(b0)
            if mult > 0 or np.isnan(mult):
                print("Latitude never returns to original position within given range.")
            else:
                predicted = self.recursive_bisection(a0, b0, reglat, position_error_bound) // 365
                print(f"Predicted year according to lat: {reference_year + predicted}")

    def recursive_bisection(self, a,b, f, error_bound):
        # Note we assume that a < b and a*b < 0
        mid = 0.5 * (a + b)

        if abs(f(mid)) <= error_bound:
            return mid

        if f(a) * f(mid) < 0:
            toreturn = self.recursive_bisection(a, mid, f, error_bound)
        else:
            toreturn = self.recursive_bisection(mid, b, f, error_bound)

        return toreturn

