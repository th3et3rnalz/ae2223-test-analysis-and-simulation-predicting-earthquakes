import _02_error_analysis.GHFiltering as GHFiltering
import _02_error_analysis.outlier_detection as Outlier
import _02_error_analysis.earthquakedetection_Edited as ED
from pandas import concat, read_csv, set_option
import os
import pickle
# from _02_error_analysis.fourier_transform import Fourier


def test_median(d14):
    outlier = Outlier.OutlierDetection(d14)
    df_no_outliers, df_earthquakes = outlier.remove_outliers_median_absolute_deviation()
    return df_no_outliers, df_earthquakes


def clean_d14(d14):
    outlier = Outlier.OutlierDetection(d14)
    without_outliers_m1, earthquakes = outlier.remove_outliers_standard_deviation(cutoff=3)

    # without_outliers_m2 = outlier.remove_outliers_method_2()
    # without_outliers_m3 = outlier.remove_outliers_method_3()
    # without_outliers_m4 = outlier.remove_outliers_method_4()

    # f = Fourier(without_outliers_m1)
    # f.periodic_variations()

    # earthquake = ED.EarthquakeDetection(d14)
    # earthquake_detection_discrete = earthquake.Detection(d14)

    # gh_filter = GHFiltering.GHFilter(d14, earthquakes, cache=False)
    # fully_cleaned_data = gh_filter.filter_all(h=0.2, g=0.05, plot=False)

    # fourier = Fourier(without_outliers_m1)
    # data_without_seasonal = fourier.periodic_variations()

    return without_outliers_m1


def get_cached_outlier_m1(d14):
    if 'cache' not in os.listdir('_02_error_analysis'):
        os.mkdir('_02_error_analysis/cache')
    if 'cleaned_dataset.csv' not in os.listdir('_02_error_analysis/cache'):
        outlier = Outlier.OutlierDetection(d14)
        cleaned, earthquakes = outlier.remove_outliers_standard_deviation(cutoff=3, analysis_type='velocity')
        cleaned.to_csv('_02_error_analysis/cache/cleaned_dataset.csv')
        pickle.dump(earthquakes, open('_02_error_analysis/cache/earthquakes_series.pickle', 'wb'))
    else:
        cleaned = read_csv('_02_error_analysis/cache/cleaned_dataset.csv', parse_dates=['datetime'])
        cleaned.set_index('datetime', inplace=True)
        cleaned.drop(columns=['Unnamed: 1'], inplace=True)
        earthquakes = pickle.load(open('_02_error_analysis/cache/earthquakes_series.pickle', 'rb'))
    return cleaned, earthquakes
