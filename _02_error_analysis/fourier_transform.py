import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import fft, interpolate, optimize
import _03_data_analysis.time_series_conversion as time_series_conversion

mpl.rc('figure', max_open_warning=0)


class Fourier:
    def __init__(self, dataset, debug=True):
        # Making a data copy of the dataset
        self.debug = debug
        self.dataset = dataset.copy()
        self.clean_dict = {}
        self.variations_dict = {}

        assert list(self.dataset.columns.values) == ['name', 'lat (deg N) (m)', 'lat_dev (cm)',
                                                     'lon (deg E) (m)', 'lon_dev (cm)', 'rad (m)', 'rad_dev (cm)',
                                                     'country']

    @staticmethod
    def fit_curve(x, a, b, c, d, e):
        return a * np.exp(-b * x ** 2) + c * np.exp(-d * x ** 2) + e

    def periodic_variations(self, plot=False):
        a = time_series_conversion.Timeseries_Converter.convert_to_time_series(self.dataset, use_cached=True)
        self.clean_dict = {}
        self.variations_dict = {}
        for key in a.keys():
            if len(a[key]['Time']) >= 50:
                interp = time_series_conversion.Timeseries_Converter.fill_station_data_gaps(a[key])
                time = interp['Time'].copy()
                originalvalues = interp['rad'].copy()
                values = interp['rad'].copy()

                s = interpolate.UnivariateSpline(time, values, s=len(time) * 100)
                bash = s(time)
                values = values - bash

                y = fft.fft(values)
                freqs = fft.fftfreq(len(values))
                order = [i for i in range(len(freqs))]
                freqs, y, order = (np.asarray(t) for t in zip(*sorted(zip(freqs, y, order))))

                frequency_magnitudes = abs(y)

                scale_factor = 1 / max(frequency_magnitudes)
                scaled_frequency_magnitudes = frequency_magnitudes * scale_factor
                # Attempt to find an optimal fit
                try:
                    popt, pcov = optimize.curve_fit(Fourier.fit_curve, freqs, scaled_frequency_magnitudes,
                                                    p0=[1, 400, 0.3, 50, 0])
                # It failed, so just append the data to the time series unaltered and take note of the station
                except:
                    print(f"Fourier curve fit failed on {key}")
                    self.clean_dict[key] = a[key]
                    continue


                fitline = self.fit_curve(freqs, *popt)

                residuals = scaled_frequency_magnitudes - fitline
                sd = np.std(residuals)

                onlypeaks = list(y)
                for i in range(len(onlypeaks)):
                    if residuals[i] < 3 * sd:
                        onlypeaks[i] = 0

                orderedonlypeaks = np.asarray(onlypeaks)
                order, onlypeaks = (np.asarray(t) for t in zip(*sorted(zip(order, onlypeaks))))

                # inverse fast fourier to flip back to the time domain
                filtered_signal = fft.ifft(onlypeaks)

                cleaned_signal = originalvalues - filtered_signal

                if plot:
                    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
                    fig.suptitle(key)

                    ax1.plot(freqs, np.abs(y), label='Spectrum magnitude of periodic variations')
                    ax1.plot(freqs, np.abs(orderedonlypeaks), label='Only peak values')
                    ax1.set_xlabel('Frequency in 1/days')
                    ax1.set_ylabel('Frequency Domain (Spectrum) Magnitude')
                    ax1.legend()

                    ax2.plot(freqs, scaled_frequency_magnitudes, label='Scaled frequency magnitudes')
                    ax2.plot(freqs, fitline, label='Frequency fit line')
                    ax2.set_xlabel('Frequency in 1/days')
                    ax2.set_ylabel('Frequency Domain (Spectrum) Magnitude')
                    ax2.legend()

                    ax3.plot(time, values, label='Periodic variation data')
                    ax3.plot(time, np.real(filtered_signal), label='Fourier series fit to periodic variation')
                    ax3.set_xlabel('Days')
                    ax3.set_ylabel('periodic Displacement')
                    ax3.legend()

                    ax4.plot(time, bash, label='Guess at a fit line')
                    ax4.plot(time, np.real(cleaned_signal), label='Data with periodic variation removed')
                    ax4.plot(time, originalvalues, label='Original input data')
                    ax4.set_xlabel('Days')
                    ax4.set_ylabel('Total Displacement')
                    ax4.legend()

                    plt.show()

                self.clean_dict[key] = interp
                self.clean_dict[key]["rad"] = cleaned_signal
                self.variations_dict[key] = filtered_signal
            else:
                print(f"Not enough datapoints to perform fourier analysis on {key}")
                self.clean_dict[key] = a[key]
        return self.clean_dict