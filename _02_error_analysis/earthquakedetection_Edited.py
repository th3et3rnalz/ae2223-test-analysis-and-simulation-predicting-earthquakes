import numpy as np
import pandas as pd
import os
import pickle
import datetime as dt


# from _01_data_ingestion.covariance import covariance_matrix

class EarthquakeDetection:
    def __init__(self, dataset, debug=True, use_cached=True):
        self.new_data_pd = {}
        self.debug = debug
        self.use_cached = use_cached

        self.dataset = dataset.copy()

        # Define 3D distance between two datapoints in spherical coordinates

    def Dis(self, lat1, lat2, lon1, lon2, rad1, rad2):
        dist = np.sqrt(rad1 ** 2 + rad2 ** 2 - 2 * rad1 * rad2 * (
                np.sin(lon1) * np.sin(lon2) * np.cos(lat1 - lat2) + np.cos(lon1) * np.cos(lon2)))

        return dist

    def Detection(self, dataset):
        self.dataset = dataset.copy()
        data = self.dataset
        stations = dataset["name"].unique()

        earthquakes_matrix = []
        outliers_matrix = []

        # Loop through the stations and each row with data
        for station in stations:
            # First we extract all the data belonging to that station out of the dataframe
            station_data = data[data["name"] == station]

            # Now let's extract lon, lat and rad values
            lon, lat, rad = [station_data[col].values for col in ("lon (deg E) (m)", "lat (deg N) (m)", "rad (m)")]

            # Now let's make the necessary calculations, requiring 3 consecutive data points
            for i in range(len(lon) - 2):
                lat1, lat2, lat3 = lat[i:i + 3]  # Latitude
                lon1, lon2, lon3 = lon[i:i + 3]  # Longitude
                rad1, rad2, rad3 = rad[i:i + 3]  # height

                distance1 = self.Dis(lat1, lat2, lon1, lon2, rad1, rad2)
                distance2 = self.Dis(lat2, lat3, lon2, lon3, rad2, rad3)
                # certain threshold, must be set in such a way that most of the earthquakes from earthquakes.csv are
                # detected (at least the large ones close to a particular station)

                if distance1 >= 0.05 and distance2 <= 0.01:
                    # Let's define these explicitly for clarity
                    date = station_data.index[i]
                    name = station

                    country = station_data["country"][i]

                    earthquakes_matrix.append([date, name, country, distance1, distance2])

                elif distance1 >= 0.05 and not distance2 <= 0.01:
                    outliers_matrix.append([station_data.index[i], *list(station_data.iloc[i].values)])

        return earthquakes_matrix, outliers_matrix

    # Define distance function between two points (3D, spherical coordinates)
    # Loop through the stations and compare the correct columns, row by row
    # Apply the distance from one data point to the other
    # If distance is larger than certain threshold: add datapoint with time and date to a list with all the detected
    # earthquakes. Compare this list with the database of earthquakes in the area in the recent timespan
