import numpy as np
import statistics
import pandas as pd


class OutlierDetection:
    def __init__(self, dataset, debug=False):
        # Making a data copy of the dataset
        self.debug = debug
        self.dataset = dataset.copy()

        assert list(self.dataset.columns.values) == ['name', 'lat (deg N) (m)', 'lat_dev (cm)', 'lon (deg E) (m)',
                                                     'lon_dev (cm)', 'rad (m)', 'rad_dev (cm)', 'country']

    def get_copy_dataset(self, velocities=False):
        dataset = self.dataset.copy()
        data = dataset.reset_index()

        if velocities:
            timedelta = pd.to_timedelta(data['datetime'].diff())
            timedelta_integer_days = np.array([val.days for val in timedelta])
            # The first value is set to the value of the second value, so that we get rid of the np.nan.
            # It's an approximation, but one worth making, imho
            timedelta_integer_days[0] = timedelta_integer_days[1]
            data['dt'] = timedelta_integer_days
            data['vlat'] = data['lat (deg N) (m)'].diff().fillna(0.) / data['dt']
            data['vlon'] = data['lon (deg E) (m)'].diff().fillna(0.) / data['dt']
            data['vrad'] = data['rad (m)'].diff().fillna(0.)
        return data

    @staticmethod
    def remove_outliers_and_detect_earthquakes(data, deviation_names, cutoff, analysis_type):
        def find_outliers_position(loc_dataframe):
            for axis in (('lat (deg N) (m)', deviation_names[0], deviation_names[1]),
                         ('lon (deg E) (m)', deviation_names[2], deviation_names[3]),
                         ('rad (m)', deviation_names[4], deviation_names[5])):
                mean = loc_dataframe[axis[1]].iloc[0]
                st_deviation = loc_dataframe[axis[2]].iloc[0]
                deviation_per_data_point = np.abs(loc_dataframe[axis[0]] - mean)
                # needs to be > since the st_deviation is 0 for months with just 1 measurement
                loc_dataframe[axis[0][0:3] + "_is_outlier"] = deviation_per_data_point > cutoff * st_deviation
            return loc_dataframe

        def find_outliers_velocity(loc_dataframe):
            for axis in (('vlat', deviation_names[0], deviation_names[1]),
                         ('vlon', deviation_names[2], deviation_names[3]),
                         ('vrad', deviation_names[4], deviation_names[5])):
                mean = loc_dataframe[axis[1]].iloc[0]
                st_deviation = loc_dataframe[axis[2]].iloc[0]
                deviation_per_data_point = np.abs(loc_dataframe[axis[0]] - mean)
                loc_dataframe[axis[0][1:4] + "_is_outlier"] = deviation_per_data_point >= cutoff * st_deviation
            return loc_dataframe

        if analysis_type == 'position':
            data = data.groupby('name').apply(find_outliers_position)
        elif analysis_type == 'velocity':
            data = data.groupby('name').apply(find_outliers_velocity)

        # Earthquake recognition - I'm assuming we want to keep the earthquakes inside of the dataset.
        def remove_outliers(date_dataframe):
            number_of_lat_outliers = date_dataframe['lat_is_outlier'].sum()
            number_of_lon_outliers = date_dataframe['lon_is_outlier'].sum()
            number_of_rad_outliers = date_dataframe['rad_is_outlier'].sum()

            if (number_of_lat_outliers >= 2) and (number_of_lon_outliers >= 2) and (number_of_rad_outliers >= 2):
                # then it must be an earthquake.
                # if is an earthquake, then we don't want to remove any outliers.
                return date_dataframe

            date_dataframe = date_dataframe.query('(lat_is_outlier == False) and (lon_is_outlier == False) and ('
                                                  'rad_is_outlier == False)')
            return date_dataframe

        def find_earthquakes(date_dataframe):
            number_of_lat_outliers = date_dataframe['lat_is_outlier'].sum()
            number_of_lon_outliers = date_dataframe['lon_is_outlier'].sum()
            number_of_rad_outliers = date_dataframe['rad_is_outlier'].sum()

            if (number_of_lat_outliers >= 2) and (number_of_lon_outliers >= 2) and (number_of_rad_outliers >= 2):
                return date_dataframe.iloc[0]

            return None

        df_outliers_removed = data.groupby('datetime').apply(remove_outliers)
        df_earthquakes_days = data.groupby('datetime').apply(find_earthquakes)
        df_earthquakes_days.dropna(inplace=True)

        # Let's now remove all the useless columns
        df_outliers_removed.drop(
            columns=['lat_is_outlier', 'lon_is_outlier', 'rad_is_outlier', 'datetime'] + deviation_names, inplace=True)
        if analysis_type == "velocity":
            df_outliers_removed.drop(columns=['dt', 'vlat', 'vlon', 'vrad'], inplace=True)
        if analysis_type == "position":
            df_outliers_removed.drop(columns=['month', 'year'], inplace=True)
        df_outliers_removed.sort_values(by=['name', 'datetime'], inplace=True)
        return df_outliers_removed, df_earthquakes_days.index

    # First method of outlier detection. It makes use of STANDARD DEVIATION
    def remove_outliers_standard_deviation(self, cutoff=2, analysis_type='velocity'):
        """
        This function checks the data for outliers by removing all rows in de dataframe
        with datapoints further than 2 (maybe 3) standard deviations away form the mean.
        for data 14

        :return: pd.Dataframe containing everything but the outliers
        """

        # The mean standard deviation and mean values will be computed for each axis, per station. and stored inside of
        # new columns.

        def calculate_mean_and_deviation_position(loc_dataframe):
            lat_pos = loc_dataframe['lat (deg N) (m)'].to_numpy()
            lon_pos = loc_dataframe['lon (deg E) (m)'].to_numpy()
            rad_pos = loc_dataframe['rad (m)'].to_numpy()

            loc_dataframe['mean_lat'] = statistics.mean(lat_pos)
            loc_dataframe['deviation_lat'] = statistics.stdev(lat_pos)
            loc_dataframe['mean_lon'] = statistics.mean(lon_pos)
            loc_dataframe['deviation_lon'] = statistics.stdev(lon_pos)
            loc_dataframe['mean_rad'] = statistics.mean(rad_pos)
            loc_dataframe['deviation_rad'] = statistics.stdev(rad_pos)

            return loc_dataframe

        def calculate_mean_and_deviation_velocity(loc_dataframe):
            lat_vel = loc_dataframe['vlat'].to_numpy()
            lon_vel = loc_dataframe['vlon'].to_numpy()
            rad_vel = loc_dataframe['vrad'].to_numpy()

            loc_dataframe['mean_lat'] = statistics.mean(lat_vel)
            loc_dataframe['deviation_lat'] = statistics.stdev(lat_vel)
            loc_dataframe['mean_lon'] = statistics.mean(lon_vel)
            loc_dataframe['deviation_lon'] = statistics.stdev(lon_vel)
            loc_dataframe['mean_rad'] = statistics.mean(rad_vel)
            loc_dataframe['deviation_rad'] = statistics.stdev(rad_vel)

            return loc_dataframe

        if analysis_type == 'position':
            # We want to work on a copy of the dataset, because otherwise we might mess up other people's work
            data = self.get_copy_dataset()
            data['year'] = data['datetime'].astype(object).dt.year
            data['month'] = data['datetime'].astype(object).dt.month
            data = data.groupby(['name', 'year', 'month']).apply(calculate_mean_and_deviation_position)
            # data = data.groupby('name').apply(calculate_mean_and_deviation_position)
        elif analysis_type == 'velocity':
            data = self.get_copy_dataset(velocities=True)
            data = data.groupby('name').apply(calculate_mean_and_deviation_velocity)

        # If either the latitude, longitude or height has an absolute standard deviation larger than [cutoff] times the
        # mean of all standard deviations then that recording is considered an outlier and that measurement will be
        # disregarded completely

        # Another goal of outlier detection is to determine whether this is due to a malfunction of the used measurement
        # devices or whether an earthquake has occurred. When an earthquake occurs, more stations (in this case we will
        # consider 3 or more) record, in the same day, the same type of outlier. This is important because if an
        # earthquake has occurred, then that measurement although initially deemed an outlier for one station it is,
        # in fact, correct and has to be considered further.

        deviation_names = ['mean_lat', 'deviation_lat', 'mean_lon', 'deviation_lon', 'mean_rad', 'deviation_rad']
        df_no_outliers, series_earthquakes = self.remove_outliers_and_detect_earthquakes(data, deviation_names,
                                                                                         cutoff=cutoff,
                                                                                         analysis_type=analysis_type)
        return df_no_outliers, series_earthquakes

    @staticmethod
    def remove_outliers_and_detect_earthquakes_MAD(data: object, median_names: object, cutoff: object, type: object) -> object:
        def find_outliers_position(loc_dataframe):
            for axis in (('lat (deg N) (m)', median_names[0], median_names[1]),
                         ('lon (deg E) (m)', median_names[2], median_names[3]),
                         ('rad (m)', median_names[4], median_names[5])):
                median = loc_dataframe[axis[1]].iloc[0]
                mad = loc_dataframe[axis[2]].iloc[0]
                val = np.abs(loc_dataframe[axis[0]] - median) / mad
                loc_dataframe[axis[0][0:3] + "_is_outlier"] = val >= cutoff
            return loc_dataframe

        def find_outliers_velocity(loc_dataframe):
            for axis in (('vlat', median_names[0], median_names[1]),
                         ('vlon', median_names[2], median_names[3]),
                         ('vrad', median_names[4], median_names[5])):
                median = loc_dataframe[axis[1]].iloc[0]
                mad = loc_dataframe[axis[2]].iloc[0]
                val = np.abs(loc_dataframe[axis[0]] - median) / mad
                loc_dataframe[axis[0][1:4] + "_is_outlier"] = val >= cutoff
            return loc_dataframe

        if type == 'position':
            data = data.groupby('name').apply(find_outliers_position)
        elif type == 'velocity':
            data = data.groupby('name').apply(find_outliers_velocity)


        def remove_outliers(date_dataframe):
            number_of_lat_outliers = date_dataframe['lat_is_outlier'].sum()
            number_of_lon_outliers = date_dataframe['lon_is_outlier'].sum()
            number_of_rad_outliers = date_dataframe['rad_is_outlier'].sum()

            if (number_of_lat_outliers >= 2) and (number_of_lon_outliers >= 2) and (number_of_rad_outliers >= 2):
                # then it must be an earthquake.
                # if is an earthquake, then we don't want to remove any outliers.
                return date_dataframe

            date_dataframe = date_dataframe.query('(lat_is_outlier == False) and (lon_is_outlier == False) and ('
                                                  'rad_is_outlier == False)')
            return date_dataframe

        def find_earthquakes(date_dataframe):
            number_of_lat_outliers = date_dataframe['lat_is_outlier'].sum()
            number_of_lon_outliers = date_dataframe['lon_is_outlier'].sum()
            number_of_rad_outliers = date_dataframe['rad_is_outlier'].sum()

            if (number_of_lat_outliers >= 2) and (number_of_lon_outliers >= 2) and (number_of_rad_outliers >= 2):
                return date_dataframe.iloc[0]

            return None


        df_outliers_removed = data.groupby('datetime').apply(remove_outliers)
        df_earthquakes_days = data.groupby('datetime').apply(find_earthquakes)
        df_earthquakes_days.dropna(inplace=True)

        # Let's now remove all the useless columns
        for df in [df_outliers_removed]:
            df.drop(columns=['lat_is_outlier', 'lon_is_outlier', 'rad_is_outlier', 'datetime'],
                        inplace=True)
            df.drop(columns=median_names, inplace=True)
            if type == 'velocity':
                df.drop(columns=['dt', 'vlat', 'vlon', 'vrad'], inplace=True)
            if type == 'position':
                df.drop(columns=['year', 'month'], inplace=True)
            df.sort_values(by=['name', 'datetime'], inplace=True)

        return df_outliers_removed, df_earthquakes_days.index

    # Second method of outlier detection. It makes use of MAD (Median Absolute Deviation).
    # Type can be v (velocity) or p (position)
    def remove_outliers_median_absolute_deviation(self, cutoff=2.5, type='position'):
        """
         This function checks the dataframe for outliers by creating a new dataframe
         with only the rows containing solely datapoints for which the distance to the median
         divided by the median absolute deviation is lower than 2.5 (maybe 3)

        :return: pd.Dataframe containing everything but the outliers
        """

        # The median and median absolute deviation will be computed for each axis, per station. and stored inside of
        # new columns.

        def calculate_median_and_mad_velocity(loc_dataframe):
            lat_v = loc_dataframe['vlat'].to_numpy()
            lon_v = loc_dataframe['vlon'].to_numpy()
            rad_v = loc_dataframe['vrad'].to_numpy()

            loc_dataframe['median_lat'] = statistics.median(lat_v)
            loc_dataframe['mad_lat'] = 1.4826 * statistics.median(
                abs(loc_dataframe['vlat'] - loc_dataframe['median_lat']))
            loc_dataframe['median_lon'] = statistics.median(lon_v)
            loc_dataframe['mad_lon'] = 1.4826 * statistics.median(
                abs(loc_dataframe['vlon'] - loc_dataframe['median_lat']))
            loc_dataframe['median_rad'] = statistics.median(rad_v)
            loc_dataframe['mad_rad'] = 1.4826 * statistics.median(
                abs(loc_dataframe['vrad'] - loc_dataframe['median_lat']))

            return loc_dataframe

        def calculate_median_and_mad_position(loc_dataframe):
            # print(loc_dataframe.shape[0])
            lat_pos = loc_dataframe['lat (deg N) (m)'].to_numpy()
            lon_pos = loc_dataframe['lon (deg E) (m)'].to_numpy()
            rad_pos = loc_dataframe['rad (m)'].to_numpy()

            loc_dataframe['median_lat'] = statistics.median(lat_pos)
            loc_dataframe['mad_lat'] = 1.4826 * statistics.median(
                abs(loc_dataframe['lat (deg N) (m)'] - loc_dataframe['median_lat']))
            loc_dataframe['median_lon'] = statistics.median(lon_pos)
            loc_dataframe['mad_lon'] = 1.4826 * statistics.median(
                abs(loc_dataframe['lon (deg E) (m)'] - loc_dataframe['median_lat']))
            loc_dataframe['median_rad'] = statistics.median(rad_pos)
            loc_dataframe['mad_rad'] = 1.4826 * statistics.median(
                abs(loc_dataframe['rad (m)'] - loc_dataframe['median_lat']))

            return loc_dataframe


        if type == 'velocity':
            # We want to work on a copy of the dataset, because otherwise we might mess up other people's work
            data = self.get_copy_dataset(velocities=True)
            data = data.groupby('name').apply(calculate_median_and_mad_velocity)


        if type == 'position':
            data = self.get_copy_dataset()
            data['year'] = (data['datetime'].astype(object).dt.year)
            data['month'] = (data['datetime'].astype(object).dt.month)
            data = data.groupby(['name','year','month']).apply(calculate_median_and_mad_position)



        # If either the latitude, longitude or height has a difference to the median devided by the mad bigger than [cutoff]
        # then that recording is considered an outlier and that measurement will be disregarded completely

        # Another goal of outlier detection is to determine whether this is due to a malfunction of the used measurement
        # devices or whether an earthquake has occurred. When an earthquake occurs, more stations (in this case we will
        # consider 3 or more) record, in the same day, the same type of outlier. This is important because if an
        # earthquake has occurred, then that measurement although initially deemed an outlier for one station it is,
        # in fact, correct and has to be considered further.

        median_names = ['median_lat', 'mad_lat', 'median_lon', 'mad_lon', 'median_rad', 'mad_rad']
        df_no_outliers, df_earthquakes = self.remove_outliers_and_detect_earthquakes_MAD(data, median_names, cutoff, type)
        return df_no_outliers, df_earthquakes

        # if type == 1:
        #     data = self.get_copy_dataset(velocities=True)
        #     data.sort_values(by=['name', 'datetime'], inplace=True)
        #
        #     median_dataframes = []
        #     mad_dataframes = []
        #     for loc_name, loc_dataframe in data.groupby('name'):
        #         loc_dataframe['median_vlat'] = statistics.median(loc_dataframe['vlat'])
        #         loc_dataframe['median_vlon'] = statistics.median(loc_dataframe['vlon'])
        #         loc_dataframe['median_vrad'] = statistics.median(loc_dataframe['vrad'])
        #
        #         median_dataframes.append(loc_dataframe)
        #
        #         loc_dataframe['lat_med_diff'] = abs(loc_dataframe['vlat'] - loc_dataframe['median_vlat'])
        #         loc_dataframe['lon_med_diff'] = abs(loc_dataframe['vlon'] - loc_dataframe['median_vlon'])
        #         loc_dataframe['rad_med_diff'] = abs(loc_dataframe['vrad'] - loc_dataframe['median_vrad'])
        #
        #         # 1.4826 normalizes the data for a normal distribution
        #         loc_dataframe['mad_vlat'] = 1.4826 * statistics.median(loc_dataframe['lat_med_diff'])
        #         loc_dataframe['mad_vlon'] = 1.4826 * statistics.median(loc_dataframe['lon_med_diff'])
        #         loc_dataframe['mad_vrad'] = 1.4826 * statistics.median(loc_dataframe['rad_med_diff'])
        #
        #         mad_dataframes.append(loc_dataframe)
        #
        #     data.concat(median_dataframes)
        #     data.concat(mad_dataframes)
        #
        #
        #
        # if type == 2:
        #     data = self.get_copy_dataset()
        #     data.sort_values(by=['name', 'datetime'], inplace=True)
        #
        #     median_dataframes = []
        #     mad_dataframes = []
        #     for loc_name, loc_dataframe in data.groupby('name'):
        #         loc_dataframe['median_lat'] = statistics.median(loc_dataframe['lat (deg N) (m)'])
        #         loc_dataframe['median_lon'] = statistics.median(loc_dataframe['lon (deg E) (m)'])
        #         loc_dataframe['median_rad'] = statistics.median(loc_dataframe['rad (m)'])
        #
        #         median_dataframes.append(loc_dataframe)
        #
        #         loc_dataframe['lat_med_diff'] = abs(loc_dataframe['lat (deg N) (m)'] - loc_dataframe['median_lat'])
        #         loc_dataframe['lon_med_diff'] = abs(loc_dataframe['lon (deg E) (m)'] - loc_dataframe['median_lon'])
        #         loc_dataframe['rad_med_diff'] = abs(loc_dataframe['rad (m)'] - loc_dataframe['median_rad'])
        #
        #         # 1.4826 normalizes the data for a normal distribution
        #         loc_dataframe['mad_lat'] = 1.4826 * statistics.median(loc_dataframe['lat_med_diff'])
        #         loc_dataframe['mad_lon'] = 1.4826 * statistics.median(loc_dataframe['lon_med_diff'])
        #         loc_dataframe['mad_rad'] = 1.4826 * statistics.median(loc_dataframe['rad_med_diff'])
        #
        #         mad_dataframes.append(loc_dataframe)
        #
        #     data.concat(median_dataframes)
        #     data.concat(mad_dataframes)
        #
        # return data
        #
        # # if type == p:
        # #     data = self.get_copy_dataset()
        # #     data.sort_values(by=['name', 'datetime'], inplace=True)

        # data = self.dataset.copy()
        # data = data.reset_index()
        #
        # # Converting the copy of the panda data frame into a numpy array so that
        # # we can perform mathematical operations on the values of the columns.
        # # The considered values will be lat_dev (cm); lon_dev (cm); rad_dev (cm)
        # arr_data = data.to_numpy()
        #
        # # Creating empty arrays to which we append the values of the above mentioned columns
        # lat_dataset = []
        # lon_dataset = []
        # rad_dataset = []
        #
        # # The median and median absolute deviation will be computed per station.
        # # Here, we are crating an array which contains the name of the station, the median and mad of the positions
        # # recorded per day for latitude, longitude and height respectively.
        # median_per_station = []
        # mad_per_station = []
        # station = arr_data[0][1]
        # for row in range(data.shape[0]):
        #     if arr_data[row][1] == station:
        #         lat_dataset = np.append(lat_dataset, arr_data[row][2])
        #         lon_dataset = np.append(lon_dataset, arr_data[row][4])
        #         rad_dataset = np.append(rad_dataset, arr_data[row][6])
        #     else:
        #         # Calculating the median
        #         median_lat = statistics.median(lat_dataset)
        #         median_lon = statistics.median(lon_dataset)
        #         median_rad = statistics.median(rad_dataset)
        #         median_tot = [station, median_lat, median_lon, median_rad]
        #         median_per_station.append(median_tot)
        #
        #         # Calculating the median absolute deviation (b=1.4826 assuming normal distribution)
        #         diff_med_lat = []
        #         diff_med_lon = []
        #         diff_med_rad = []
        #         for r in range(data.shape[0]):
        #             diff_med_lat.append(abs(arr_data[r][2] - median_lat))
        #             diff_med_lon.append(abs(arr_data[r][4] - median_lat))
        #             diff_med_rad.append(abs(arr_data[r][6] - median_lat))
        #
        #         mad_lat = (1.4826 * statistics.median(diff_med_lat))
        #         mad_lon = (1.4826 * statistics.median(diff_med_lon))
        #         mad_rad = (1.4826 * statistics.median(diff_med_rad))
        #         mad_tot = [station, mad_lat, mad_lon, mad_rad]
        #         mad_per_station.append(mad_tot)
        #
        #         station = arr_data[row][1]
        #         lat_dataset = []
        #         lon_dataset = []
        #         rad_dataset = []
        #
        #     # Adding last station
        #     if arr_data[row][1] == 'UUMK':
        #         lat_dataset = np.append(lat_dataset, arr_data[row][2])
        #         lon_dataset = np.append(lon_dataset, arr_data[row][4])
        #         rad_dataset = np.append(rad_dataset, arr_data[row][6])
        #
        # median_lat = statistics.median(lat_dataset)
        # median_lon = statistics.median(lon_dataset)
        # median_rad = statistics.median(rad_dataset)
        # median_tot = [station, median_lat, median_lon, median_rad]
        # median_per_station.append(median_tot)
        #
        # diff_med_lat = []
        # diff_med_lon = []
        # diff_med_rad = []
        # for r in range(data.shape[0]):
        #     diff_med_lat.append(abs(arr_data[r][2] - median_lat))
        #     diff_med_lon.append(abs(arr_data[r][4] - median_lat))
        #     diff_med_rad.append(abs(arr_data[r][6] - median_lat))
        #
        # mad_lat = (1.4826 * statistics.median(diff_med_lat))
        # mad_lon = (1.4826 * statistics.median(diff_med_lon))
        # mad_rad = (1.4826 * statistics.median(diff_med_rad))
        # mad_tot = [station, mad_lat, mad_lon, mad_rad]
        # mad_per_station.append(mad_tot)
        #
        # # Create new numpy array to which we append the outliers. Which we will later use to extract the outliers from
        # # the dataset.
        # mad_outliers = []
        # mad_numb_outliers = []
        #
        # # If the difference between the median of either the latitude, longitude or height divided by
        # # the median absolute deviation is bigger than 2.5 then that recording is considered an outlier
        # # and that measurement will be disregarded completely
        #
        # # Initialising the first station to be analysed
        # station = arr_data[0][0]
        # station_number = 0
        #
        # # Another goal of outlier detection is to determine whether this is due to a malfunction of the used measurement
        # # devices or whether an earthquake has occurred. When an earthquake occurs, more stations (in this case we will
        # # consider 3 or more) record, in the same day, the same type of outlier). This is important because if an
        # # earthquake has occurred, then that measurement although initially deemed an outlier for one station it is,
        # # in fact, correct and has to be considered further.
        # mad_outlier_type = []
        # mad_outlier_type_all_cases = []
        # for row in range(data.shape[0]):
        #     if arr_data[row][1] == station:
        #         check = 0
        #         mad_outlier_lat = 0
        #         mad_outlier_lon = 0
        #         mad_outlier_rad = 0
        #         # print(station_number)
        #         if (abs(arr_data[row][2] - median_per_station[station_number][1])) / (
        #         mad_per_station[station_number][1]) >= 2.5:
        #             if check == 0:
        #                 mad_outliers.append(arr_data[row])
        #                 mad_numb_outliers = np.append(mad_numb_outliers, row)
        #                 check = 1
        #             mad_outlier_lat = 1
        #         if (abs(arr_data[row][4] - median_per_station[station_number][2])) / (
        #         mad_per_station[station_number][2]) >= 2.5:
        #             if check == 0:
        #                 mad_outliers.append(arr_data[row])
        #                 mad_numb_outliers = np.append(mad_numb_outliers, row)
        #                 check = 1
        #             mad_outlier_lon = 2
        #         if (abs(arr_data[row][6] - median_per_station[station_number][3])) / (
        #         mad_per_station[station_number][3]) >= 2.5:
        #             if check == 0:
        #                 mad_outliers.append(arr_data[row])
        #                 mad_numb_outliers = np.append(mad_numb_outliers, row)
        #                 check = 1
        #             mad_outlier_rad = 3
        #         if check == 1:
        #             mad_outlier_type = [mad_outlier_lat, mad_outlier_lon, mad_outlier_rad]
        #     else:
        #         mad_outlier_type_all_cases.append(mad_outlier_type)
        #         station = arr_data[row][1]
        #         station_number = station_number + 1
        #         if station_number == 30:
        #             break
        #
        # # Earthquake recognition
        #
        # # Crating a new array with the filtered data (without outliers)
        # mad_arr_new_data = []
        # for i in range(data.shape[0]):
        #     verify = 0
        #     for j in range(mad_numb_outliers.shape[0]):
        #         if i == mad_numb_outliers[j]:
        #             verify = 1
        #         else:
        #             continue
        #     if verify == 0:
        #         mad_arr_new_data.append(arr_data[i])
        #     else:
        #         continue
        #
        # # Converting the new data set to a panda data frame
        # df2 = pd.DataFrame(mad_arr_new_data, columns=['datetime',
        #                                               'name',
        #                                               'lat (deg N) (m)',
        #                                               'lat_dev (cm)',
        #                                               'lon (deg E) (m)',
        #                                               'lon_dev (cm)',
        #                                               'rad (m)',
        #                                               'rad_dev (cm)',
        #                                               'country'])
        #
        # df2 = df2.set_index('datetime')
        # print('The new filtered (Based on Median Absolute Deviation Method) data set without outliers is:')
        # return df2

    # # Thirds method of outlier detection. It makes use of Sn Estimator
    # def remove_outliers_sn_estimator(self):
    #     """
    #      This function checks the dataframe for outliers by creating a new dataframe
    #      with only the rows containing solely datapoints for which median of the distance between that point
    #      and all other datapoints divided by the median of this estimator for all datapoints is lower than 2.5 (maybe 3)
    #
    #     :return: pd.Dataframe containing everything but the outliers
    #
    #     """
    #
    #     data = self.dataset.copy()
    #     data.reset_index(inplace=True)
    #
    #     # Converting the copy of the panda data frame into a numpy array so that we can perform mathematical operations
    #     # on the values of the columns. The considered values will be lat_dev (cm); lon_dev (cm); rad_dev (cm)
    #     arr_data = data.to_numpy()
    #
    #     # Creating empty arrays to which we append the values of the above mentioned columns
    #     lat_dataset = []
    #     lon_dataset = []
    #     rad_dataset = []
    #
    #     diff_median_per_station = []
    #     med_diff_median_per_station = []
    #     station = arr_data[0][1]
    #     for row in range(data.shape[0]):
    #         if arr_data[row][1] == station:
    #             lat_dataset = np.append(lat_dataset, arr_data[row][2])
    #             lon_dataset = np.append(lon_dataset, arr_data[row][4])
    #             rad_dataset = np.append(rad_dataset, arr_data[row][6])
    #         else:
    #             lat_diff_median = []
    #             lon_diff_median = []
    #             rad_diff_median = []
    #             for i in range(lat_dataset.shape[0]):
    #                 difference = []
    #                 for j in range(lat_dataset.shape[0]):
    #                     diff = abs(lat_dataset[i] - lat_dataset[j])
    #                     difference.append(diff)
    #
    #                 lat_diff_median.append(statistics.median(difference))
    #
    #             for k in range(lon_dataset.shape[0]):
    #                 difference = []
    #                 for o in range(lon_dataset.shape[0]):
    #                     diff = abs(lon_dataset[k] - lon_dataset[o])
    #                     difference.append(diff)
    #
    #                 lon_diff_median.append(statistics.median(difference))
    #
    #             for m in range(rad_dataset.shape[0]):
    #                 difference = []
    #                 for n in range(rad_dataset.shape[0]):
    #                     diff = abs(rad_dataset[m] - rad_dataset[n])
    #                     difference.append(diff)
    #
    #                 rad_diff_median.append(statistics.median(difference))
    #
    #             diff_median_tot = [station, lat_diff_median, lon_diff_median, rad_diff_median]
    #             diff_median_per_station.append(diff_median_tot)
    #
    #             # Calculate the median of the medians of the differences. (with a correction factor of 1.1926)
    #             med_lat_diff_median = 1.1926 * statistics.median(lat_diff_median)
    #             med_lon_diff_median = 1.1926 * statistics.median(lon_diff_median)
    #             med_rad_diff_median = 1.1926 * statistics.median(rad_diff_median)
    #             med_diff_median_tot = [station, med_lat_diff_median, med_lon_diff_median, med_rad_diff_median]
    #             med_diff_median_per_station.append(med_diff_median_tot)
    #             station = arr_data[row][1]
    #             lat_dataset = []
    #             lon_dataset = []
    #             rad_dataset = []
    #
    #         # Adding last station
    #         if arr_data[row][1] == 'UUMK':
    #             lat_dataset = np.append(lat_dataset, arr_data[row][2])
    #             lon_dataset = np.append(lon_dataset, arr_data[row][4])
    #             rad_dataset = np.append(rad_dataset, arr_data[row][6])
    #
    #     lat_diff_median = []
    #     lon_diff_median = []
    #     rad_diff_median = []
    #     for i in range(lat_dataset.shape[0]):
    #         difference = []
    #         for j in range(lat_dataset.shape[0]):
    #             diff = abs(lat_dataset[i] - lat_dataset[j])
    #             difference.append(diff)
    #
    #         lat_diff_median.append(statistics.median(difference))
    #
    #     for k in range(lon_dataset.shape[0]):
    #         difference = []
    #         for o in range(lon_dataset.shape[0]):
    #             diff = abs(lon_dataset[k] - lon_dataset[o])
    #             difference.append(diff)
    #
    #         lon_diff_median.append(statistics.median(difference))
    #
    #     for m in range(rad_dataset.shape[0]):
    #         difference = []
    #         for n in range(rad_dataset.shape[0]):
    #             diff = abs(rad_dataset[m] - rad_dataset[n])
    #             difference.append(diff)
    #
    #         rad_diff_median.append(statistics.median(difference))
    #
    #     diff_median_tot = [station, lat_diff_median, lon_diff_median, rad_diff_median]
    #     diff_median_per_station.append(diff_median_tot)
    #
    #     # Calculate the median of the medians of the differences. (with a correction factor of 1.1926)
    #     med_lat_diff_median = 1.1926 * statistics.median(lat_diff_median)
    #     med_lon_diff_median = 1.1926 * statistics.median(lon_diff_median)
    #     med_rad_diff_median = 1.1926 * statistics.median(rad_diff_median)
    #     med_diff_median_tot = [station, med_lat_diff_median, med_lon_diff_median, med_rad_diff_median]
    #     med_diff_median_per_station.append(med_diff_median_tot)
    #
    #     # Create new numpy array to which we append the outliers. Which we will later use to extract the outliers
    #     # from the dataset.
    #     sn_outliers = []
    #     sn_numb_outliers = []
    #
    #     # If the median of the difference between a datapoint and all other datapoints
    #     # for either the latitude, longitude or height
    #     # divided by the median of all medians of the differences
    #     # is bigger than 2.5 then that recording is considered an outlier and that measurement will be
    #     # disregarded completely
    #
    #     # Initialising the first station to be analysed
    #     station = arr_data[0][0]
    #     station_number = 0

    #     # Another goal of outlier detection is to determine whether this is due to a malfunction of the used measurement
    #     # devices or whether an earthquake has occurred. When an earthquake occurs, more stations (in this case we will
    #     # consider 3 or more) record, in the same day, the same type of outlier). This is important because if an
    #     # earthquake has occurred, then that measurement although initially deemed an outlier for one station it is,
    #     # in fact, correct and has to be considered further.
    #     sn_outlier_type = []
    #     sn_outlier_type_all_cases = []
    #     # If the difference of either the latitude, longitude or height divided by the median of all differences
    #     # is bigger than 2.5, the row is stored in the outlier array.
    #
    #     for row in range(data.shape[0]):
    #         if arr_data[row][1] == station:
    #             check = 0
    #             sn_outlier_lat = 0
    #             sn_outlier_lon = 0
    #             sn_outlier_rad = 0
    #
    #             for r in range(len(diff_median_per_station[station_number][1])):
    #                 if (diff_median_per_station[station_number][1])[r] / (
    #                         med_diff_median_per_station[station_number][1]) >= 2.5:
    #                     if check == 0:
    #                         sn_outliers.append(arr_data[row])
    #                         sn_numb_outliers = np.append(sn_numb_outliers, row)
    #                         check = 1
    #                     sn_outlier_lat = 1
    #                 if (diff_median_per_station[station_number][2])[r] / (
    #                         med_diff_median_per_station[station_number][2]) >= 2.5:
    #                     if check == 0:
    #                         sn_outliers.append(arr_data[row])
    #                         sn_numb_outliers = np.append(sn_numb_outliers, row)
    #                         check = 1
    #                     sn_outlier_lon = 2
    #                 if (diff_median_per_station[station_number][1])[r] / med_diff_median_per_station[station_number][
    #                     1] >= 2.5:
    #                     if check == 0:
    #                         sn_outliers.append(arr_data[row])
    #                         sn_numb_outliers = np.append(sn_numb_outliers, row)
    #                         check = 1
    #                     sn_outlier_rad = 3
    #                 if check == 1:
    #                     sn_outlier_type = [sn_outlier_lat, sn_outlier_lon, sn_outlier_rad]
    #
    #         else:
    #             sn_outlier_type_all_cases.append(sn_outlier_type)
    #             station = arr_data[row][1]
    #             station_number = station_number + 1
    #
    #             if station_number == 30:
    #                 break
    #
    #     # Earthquake recognition
    #
    #     sn_arr_new_data = []
    #     for i in range(data.shape[0]):
    #         verify = 0
    #         for j in range(sn_numb_outliers.shape[0]):
    #             if i == sn_numb_outliers[j]:
    #                 verify = 1
    #             else:
    #                 continue
    #         if verify == 0:
    #             sn_arr_new_data.append(arr_data[i])
    #         else:
    #             continue
    #
    #     # Converting the new data set to a panda data frame
    #     df3 = pd.DataFrame(sn_arr_new_data, columns=['datetime',
    #                                                  'name',
    #                                                  'lat (deg N) (m)',
    #                                                  'lat_dev (cm)',
    #                                                  'lon (deg E) (m)',
    #                                                  'lon_dev (cm)',
    #                                                  'rad (m)',
    #                                                  'rad_dev (cm)',
    #                                                  'country'])
    #
    #     df3 = df3.set_index('datetime')
    #     print(
    #         'The new filtered (Based on Median of the Differences between Points Method) data set without outliers is:')
    #     return df3
    #
    # # This fourth method of outlier detection. Uses the median absolute deviations of the velocity
    # def remove_outliers_median_absolute_deviation_velocity(self):
    #     """
    #       This function checks the dataframe for outliers by creating a new dataframe
    #      with only the rows containing solely datapoints for which the distance of velocity of the station to the median
    #      velocity divided by the median absolute deviation is lower than 2.5 (maybe 3)
    #
    #     :return: pd.Dataframe containing everything but the outliers
    #
    #     """
    #     data = self.get_copy_dataset(velocities=True)
    #     data.sort_values(by=['name', 'datetime'], inplace=True)
    #
    #     return data
    #     # create an array with the data
    #     data = data.reset_index()
    #     arr_data = data.to_numpy()
    #
    #     # compute the median of the velocities for each station
    #     stations_median = []
    #
    #     for loc_name, loc_dataframe in data.groupby('name'):
    #         lat_median = statistics.median(loc_dataframe['vlat'].to_numpy())
    #         lon_median = statistics.median(loc_dataframe['vlon'].to_numpy())
    #         rad_median = statistics.median(loc_dataframe['vrad'].to_numpy())
    #         tot_median = [loc_name, lat_median, lon_median, rad_median]
    #         stations_median.append(tot_median)
    #
    #     # compute the median absolute deviation of the velocities
    #     stations_mad = []
    #
    #     for loc_name, loc_dataframe in data.groupby('name'):
    #         lat_med_diff = []
    #         lon_med_diff = []
    #         rad_med_diff = []
    #
    #         for i in range(len(stations_median)):
    #             lat_med_diff = np.append(lat_med_diff, abs(loc_dataframe['vlat'] - stations_median[i][1]))
    #             lon_med_diff = np.append(lon_med_diff, abs(loc_dataframe['vlon'] - stations_median[i][2]))
    #             rad_med_diff = np.append(rad_med_diff, abs(loc_dataframe['vrad'] - stations_median[i][3]))
    #
    #         lat_mad = statistics.median(lat_med_diff)
    #         lon_mad = statistics.median(lon_med_diff)
    #         rad_mad = statistics.median(rad_med_diff)
    #         tot_mad = [loc_name, lat_mad, lon_mad, rad_mad]
    #         stations_mad.append(tot_mad)
    #
    #     # Create new numpy array to which we append the outliers and the number of the row that contains an outlier in
    #     # the initial data set. We will use the latter to extract the outliers from the data set
    #     outliers = []
    #     numb_outliers = []
    #
    #     # If either the velocity in latitude, longitude or height differs more from the median of the velocity than
    #     # 2.5 times the median absolute deviation this datapoint is considered an outlier
    #
    #     # Initialising the first station to be analysed
    #     station = stations_mad[0][0]
    #     station_number = 0
    #
    #     outlier_type = []
    #     outlier_type_all_cases = []
    #     for row in range(data.shape[0]):
    #         if arr_data[row][1] == station:
    #             check = 0
    #             outlier_lat = 0
    #             outlier_lon = 0
    #             outlier_rad = 0
    #
    #             # b=1.4826 assuming normal distribution
    #             if abs(arr_data[row][13] - stations_median[station_number][1]) \
    #                     / (1.4826 * stations_mad[station_number][1]) >= 2.5:
    #                 if check == 0:
    #                     outliers.append(arr_data[row])
    #                     numb_outliers = np.append(numb_outliers, row)  # Position of outliers in the data set
    #                     check = 1
    #                 outlier_lat = 1
    #             if abs(arr_data[row][14] - stations_median[station_number][2]) \
    #                     / (1.4826 * stations_mad[station_number][2]) >= 2.5:
    #                 if check == 0:
    #                     outliers.append(arr_data[row])
    #                     numb_outliers = np.append(numb_outliers, row)
    #                     check = 1
    #                 outlier_lon = 2
    #             if abs(arr_data[row][15] - stations_median[station_number][3]) \
    #                     / (1.4826 * stations_mad[station_number][3]) >= 2.5:
    #                 if check == 0:
    #                     outliers.append(arr_data[row])
    #                     numb_outliers = np.append(numb_outliers, row)
    #                     check = 1
    #                 outlier_rad = 3
    #
    #             if check == 1:
    #                 outlier_type = [outlier_lat, outlier_lon, outlier_rad]
    #         else:
    #             outlier_type_all_cases.append(outlier_type)
    #             station = arr_data[row][1]
    #             station_number = station_number + 1
    #
    #     # Crating a new array with the filtered data (without outliers)
    #     arr_new_data = []
    #     for i in range(data.shape[0]):
    #         verify = 0
    #         for j in range(len(numb_outliers)):
    #             if i == numb_outliers[j]:
    #                 verify = 1
    #             else:
    #                 continue
    #         if verify == 0:
    #             arr_new_data.append(arr_data[i])
    #         else:
    #             continue
    #
    #     # Converting the new data set to a panda data frame
    #     df = pd.DataFrame(arr_new_data, columns=['datetime',
    #                                              'name',
    #                                              'lat (deg N) (m)',
    #                                              'lat_dev (cm)',
    #                                              'lon (deg E) (m)',
    #                                              'lon_dev (cm)',
    #                                              'rad (m)',
    #                                              'rad_dev (cm)',
    #                                              'country'])
    #
    #     df4 = df.set_index('datetime')
    #     print('The new filtered (Based on Standard Deviation Method) data set without outliers is:')
    #     return df4
    #
    #     # for loc_name, loc_dataframe in data.groupby('name'):
    #     #     for i in range(len(stations_mad)):
    #     #         if abs((abs(arr_data[row][2] - median_per_station[station_number][1])) / (
    #     #             mad_per_station[station_number][1])) >= 2.5:
    #
    #     # return len(data['name'])
