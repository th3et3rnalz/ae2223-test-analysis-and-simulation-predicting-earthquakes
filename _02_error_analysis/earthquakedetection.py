import numpy as np
import pandas as pd
import os
import pickle
import datetime as dt

#from _01_data_ingestion.covariance import covariance_matrix

class EarthquakeDetection:
    def __init__(self, dataset, debug=True, use_cached=True):
        self.new_data_pd = {}
        self.debug = debug
        self.use_cached = use_cached

        self.dataset = dataset.copy()

        #Define 3D distance between two datapoints in spherical coordinates
    def Dis(self, lat1, lat2 ,lon1, lon2, rad1, rad2):
        dist = np.sqrt(rad1**2 + rad2**2 - 2*rad1*rad2*(np.sin(lon1)*np.sin(lon2)*np.cos(lat1-lat2)+np.cos(lon1)*np.cos(lon2)))

        return dist

    def Detection(self, dataset):
        self.dataset = dataset.copy()
        data = self.dataset
        data = data.reset_index()
        # array_data = data.to_numpy()
        stations = dataset["name"].unique()

        detected_earthquakes = []
        detected_outliers = []
        earthquakes_matrix = []
        outliers_matrix = []

        #Loop through the stations and each row with data
        for i in range(len(stations)):
            for row in range(data.shape[0]-2):
                lat1, lat2, lat3 = data[row][2], data[row + 1][2], data[row+2][2] #Latitude
                lon1, lon2, lon3 = data[row][4], data[row + 1][4], data[row+2][4] #Longitude
                rad1, rad2, rad3 = data[row][6], data[row + 1][6], data[row+2][6] #height

            distance1 = self.Dis(lat1,lat2,lon1,lon2,rad1,rad2)
            distance2 = self.Dis(lat2,lat3,lon2,lon3,rad2,rad3)
            #certain threshold, must be set in such a way that most of the earthquakes from earthquakes.csv are detected
            # (at least the large ones close to a particular station)
            
            if distance1 >= 0.05 and distance2 <= 0.01:
                detected_earthquakes.append(data[row][0])
                detected_earthquakes.append(data[row][1])
                detected_earthquakes.append(data[row][8])
                detected_earthquakes.append(distance1)
                detected_earthquakes.append(distance2)
                #Store the detected earthquakes in a matrix to have a better overview

                while detected_earthquakes != []:
                    earthquakes_matrix.append(detected_earthquakes[:5])

                    detected_earthquakes = detected_earthquakes[5:]

            if distance1 >= 0.05 and not distance2 <= 0.01:
                detected_outliers.append(data[row][0])
                detected_outliers.append(data[row][1])
                detected_outliers.append(data[row][2])
                detected_outliers.append(data[row][3])
                detected_outliers.append(data[row][4])
                detected_outliers.append(data[row][5])
                detected_outliers.append(data[row][6])
                detected_outliers.append(data[row][7])
                detected_outliers.append(data[row][8])

                while detected_outliers != []:
                    outliers_matrix.append(detected_outliers[:9])

                    detected_outliers = detected_outliers[9:]

        return earthquakes_matrix, outliers_matrix

    #Define distance function between two points (3D, spherical coordinates)
    #Loop through the stations and compare the correct columns, row by row
    #Apply the distance from one data point to the other
    #If distance is larger than certain threshold: add datapoint with time and date to a list with all the detected earthquakes
    #Compare this list with the database of earthquakes in the area in the recent timespan

