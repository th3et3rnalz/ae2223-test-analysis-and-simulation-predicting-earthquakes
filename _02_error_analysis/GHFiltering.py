import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import os


class GHFilter:
    def __init__(self, dataset, earthquakes, cache=False, debug=False):
        self.dataset = dataset
        self.cache = cache
        self.debug = debug
        self.earthquakes = earthquakes

    def filter(self, h, g, mean, deviation, t):
        filtered = [mean[0]]

        # This dx is an initial guess, it will be further updated
        dx = sum([(mean[0] - mean[n]) / (t[0] - t[n]).days for n in range(1, 11)]) / 10

        for n in range(0, mean.shape[0] - 1):  # n refers to the current step, where n+1 is the one to be estimated
            dt = (t[n + 1] - t[n]).days

            x_p = filtered[n] + dx * dt  # Estimate/prediction of x in the next time step

            residual = mean[n + 1] - x_p  # The difference between our prediction and what was measured

            # Our estimate for the next time step. In the following steps we will assume that this is the true value.
            x_next = x_p + self.f_special(dt, h, deviation[n + 1]) * residual
            filtered.append(x_next)

            # We must adapt our prediction variable dx based on the difference between our prediction and found value.
            dx += g * residual / dt

        return filtered

    @staticmethod
    def plot(output_from_filter, input_to_filter):
        # both of these are pandas dataframes
        fig, ax = plt.subplots(3, sharex=True)
        ax[0].plot(output_from_filter.index.to_pydatetime(), output_from_filter['lat (deg N) (m)'], label='filtered')
        ax[0].plot(output_from_filter.index.to_pydatetime(), input_to_filter['lat (deg N) (m)'], label='original')
        ax.flat[0].set(ylabel='lat (deg N) [m]')

        ax[1].plot(output_from_filter.index.to_pydatetime(), output_from_filter['lon (deg E) (m)'])
        ax[1].plot(output_from_filter.index.to_pydatetime(), input_to_filter['lon (deg E) (m)'])
        ax.flat[1].set(ylabel='lon (deg E) [m]')

        ax[2].plot(output_from_filter.index.to_pydatetime(), output_from_filter['rad (m)'])
        ax[2].plot(output_from_filter.index.to_pydatetime(), input_to_filter['rad (m)'])
        ax.flat[2].set(xlabel='time [yr]', ylabel='rad [m]')

        fig.savefig('_04_visualisations/Figures/' + input_to_filter['name'][0] + '_time_series.pdf', format='pdf')
        ax[0].set_title(input_to_filter['name'][0])
        fig.legend()
        fig.show()

    def filter_all(self, h, g, plot=False):
        if self.debug:
            print('before:\n', self.dataset)
        locations = self.dataset['name'].unique()
        cleaned_dataframes = []

        if self.cache:
            os.chdir('_02_error_analysis')
            if 'cache' in os.listdir():
                os.chdir('cache')
                if 'kalman_filtered_data.csv' in os.listdir():
                    return pd.read_csv('kalman_filtered_data.csv')
                os.chdir('..')
            os.chdir('..')

        for loc in locations:
            data = self.dataset[self.dataset['name'] == loc]
            data = data.loc[~data.index.duplicated(keep='first')]
            t = list(data.index.to_pydatetime())

            axes = [['lat (deg N) (m)', 'lat_dev (cm)'],
                    ['lon (deg E) (m)', 'lon_dev (cm)'],
                    ['rad (m)', 'rad_dev (cm)']]

            if len(data[axes[0][0]]) < 10:
                continue

            columns_dict_all_values = {'name': loc, 'datetime': t}

            for axis in axes:
                mean = data[axis[0]].to_numpy()
                standard_deviation = data[axis[1]].to_numpy()

                filtered_mean = self.filter(h=h, g=g, mean=mean, deviation=standard_deviation, t=t)

                columns_dict_all_values[axis[0]] = filtered_mean
                columns_dict_all_values[axis[1]] = standard_deviation

            df = pd.DataFrame(columns_dict_all_values)
            df['country'] = data['country'].values[0]
            df.set_index('datetime', inplace=True)
            cleaned_dataframes.append(df)
            if plot:
                self.plot(df, data)

        resulting_dataframe = pd.concat(cleaned_dataframes)

        os.chdir('_02_error_analysis')
        if 'cache' not in os.listdir():
            os.mkdir('cache')
        os.chdir('cache')
        resulting_dataframe.to_csv('kalman_filtered_data.csv')
        os.chdir('..')
        os.chdir('..')

        if self.debug:
            print('after:\n', resulting_dataframe)

        assert (self.dataset.columns == resulting_dataframe.columns).all()

        return resulting_dataframe

    # This function determines how well we follow the data depending on the time-step is and the SD
    @staticmethod
    def f_special(dt, h, st_dev, max=0.8, scale_time=50):
        assert max > h
        h_guess = h
        if dt > 1:
            h_guess += (1 - np.exp((1 - dt) / scale_time)) * (max - h)

        # Now we must take into account that we trust the sensor less if the SD of the sensor is higher
        sqrt_sd = 1 - np.exp(-1*st_dev*40)

        return h_guess * sqrt_sd

