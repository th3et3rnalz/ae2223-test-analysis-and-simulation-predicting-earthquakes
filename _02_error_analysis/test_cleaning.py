import unittest
from _01_data_ingestion import get_d14
from _02_error_analysis.outlier_detection import OutlierDetection


class TestDataIngestion(unittest.TestCase):
    def test_outlier_detection_sd(self):
        data = get_d14()
        outlier = OutlierDetection(data)

        # First we try position. THE POSITION ANALYSIS REALLY SHOULD NOT BE USED
        # no_outliers, earthquakes = outlier.remove_outliers_standard_deviation(cutoff=3, input='position')
        #
        # # Just checking some logical assumptions that I am making about the data
        #
        # #  1) There is between 0 and 5% of outliers
        # self.assertLess(no_outliers.shape[0] / data.shape[0], 1)
        # self.assertGreater(no_outliers.shape[0] / data.shape[0], 0.95)
        #
        # #  2) Between 0 and 3% of days are earthquakes
        # self.assertLess(len(earthquakes) / len(data.index.unique()), 0.02)
        # self.assertGreater(earthquakes.shape[0] / data.shape[0], 0)

        # Then the velocity.
        no_outliers, earthquakes = outlier.remove_outliers_standard_deviation(cutoff=2, analysis_type='velocity')

        # Just checking some logical assumptions that I am making about the data

        #  1) There is between 0 and 5% of outliers
        self.assertLess(no_outliers.shape[0] / data.shape[0], 1)
        self.assertGreater(no_outliers.shape[0] / data.shape[0], 0.98)

        #  2) Between 0 and 2% of days are earthquakes
        self.assertLess(len(earthquakes) / len(data.index.unique()), 0.02)
        self.assertGreater(earthquakes.shape[0] / data.shape[0], 0)

